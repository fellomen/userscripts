# Userscripts

A place to dump my userscripts/custom CSS files.

### discord.com
- Install: https://gitlab.com/fellomen/userscripts/-/raw/master/discord.user.js
- README/Wiki: https://gitlab.com/fellomen/userscripts/-/wikis/discord.user.js
- `discord.user.css` can probably be used stand-alone with a custom css plugin, but I didn't try that.

### fofu (file utils)
- Install: https://gitlab.com/fellomen/userscripts/-/raw/master/fofu.user.js
- README/Wiki: https://gitlab.com/fellomen/userscripts/-/wikis/fofu.user.js

### kemono.party/coomer.party
- Install: https://gitlab.com/fellomen/userscripts/-/raw/master/kemonoparty.user.js
- README/Wiki: https://gitlab.com/fellomen/userscripts/-/wikis/kemonoparty.user.js

### twitter.com
- Install: https://gitlab.com/fellomen/userscripts/-/raw/master/twitter.user.js
- README/Wiki: https://gitlab.com/fellomen/userscripts/-/wikis/twitter.user.js

### bsky.app
- Install: https://gitlab.com/fellomen/userscripts/-/raw/master/bsky.user.js
- README/Wiki: https://gitlab.com/fellomen/userscripts/-/wikis/bsky.user.js
