// ==UserScript==
// @name         Fell Omen's File Utils
// @namespace    fellomen
// @version      1.1.0
// @description  Add some utilities to raw files in your browser. As opposed to cooked files.
// @author       fellomen
// @match        *://*/*.*
// @match        *://*/*@*
// @noframes
// @grant        GM_download
// @grant        GM.download
// @grant        GM_getValue
// @grant        GM.getValue
// @grant        GM_setValue
// @grant        GM.setValue
// @grant        GM_registerMenuCommand
// @grant        GM.registerMenuCommand
// @grant        GM_setClipboard
// @grant        GM.setClipboard
// @require      https://gitlab.com/fellomen/userscripts/-/raw/compat.js-2.0.0/tools/compat.js
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @updateURL    https://gitlab.com/fellomen/userscripts/-/raw/master/fofu.user.js
// @downloadURL  https://gitlab.com/fellomen/userscripts/-/raw/master/fofu.user.js
// @supportURL   https://gitlab.com/fellomen/userscripts
// @homepageURL  https://gitlab.com/fellomen/userscripts
// ==/UserScript==
(() => {
  "use strict";

  if (!document.head.querySelector("link[rel='stylesheet'][href^='resource://']")) {
    // Assumption: Only raw file pages use the resource://-based stylesheets like
    // TopLevelVideoDocument.css, TopLevelImageDocument.css, plaintext.css and pdf.js/web/viewer.css
    console.info("FOFU.user.js: Not running on raw file page. Exiting.");
    return;
  }

  const Colors = {
    Foreground: "#CEB277",
    Background: "#333333EE",
    Success: "green",
    Failure: "red",
  };

  const ConfigKeys = {
    Enable_Download_Keybind: "Enable_Download_Keybind",
    Enable_Clipboard_Keybind: "Enable_Clipboard_Keybind",
    Use_Override_Filenames: "Use_Override_Filenames",
    Enable_Resizer: "Enable_Resizer",
    Show_UI: "Show_UI",
    File_Name_Template: "File_Name_Template",
    Per_Domain_File_Name_Templates: "Per_Domain_File_Name_Templates",
  };

  const TemplateValues = {
    FileName: "{filename}",
    Host: "{host}",
    Path: "{path}",
    DownloadTimestamp: "{dl_timestamp}",
  };

  const config = initConfig(() => {
    GM_Compat.registerMenuCommand("Script settings", () => config.open(), "p");
    document.addEventListener("keydown", evt => {
      if (evt.code === "KeyS" && evt.ctrlKey && config.get(ConfigKeys.Enable_Download_Keybind)) {
        evt.preventDefault();
        download();
      } else if (
        evt.code === "KeyC" &&
        evt.ctrlKey &&
        config.get(ConfigKeys.Enable_Clipboard_Keybind)
      ) {
        evt.preventDefault();
        copyToClipboard();
      }
    });
    if (config.get(ConfigKeys.Enable_Resizer)) {
      resize();
    }
    if (config.get(ConfigKeys.Show_UI)) {
      createUI();
    }
  });

  /**
   * Returns the file name for saving the file at the current `document.location`.
   * Takes care of properly handling override file names.
   * @returns {!string}
   */
  function getFileName() {
    if (document.location.hash && config.get(ConfigKeys.Use_Override_Filenames)) {
      return document.location.hash.substring(1);
    }
    return document.location.pathname.split("/").pop();
  }

  /**
   * Creates and appends the download UI element(s) to `document.body`.
   */
  function createUI() {
    const downloadButton = document.createElement("span");
    downloadButton.id = "fufo-download-state";
    downloadButton.innerText = "Ready!";
    downloadButton.addEventListener("click", () => download());
    downloadButton.style.position = "absolute";
    downloadButton.style.bottom = "4px";
    downloadButton.style.left = "4px";
    downloadButton.style.overflow = "hidden";
    downloadButton.style.padding = "4px";
    downloadButton.style.margin = "4px";
    downloadButton.style.border = "2px solid " + Colors.Foreground;
    downloadButton.style.outline = "4px solid " + Colors.Background;
    downloadButton.style.background = Colors.Background;
    downloadButton.style.color = Colors.Foreground;
    document.body.appendChild(downloadButton);
  }

  function download() {
    const url = document.location.href;
    const name = decodeURI(getFileName(url).replace(/[\/\\]/g, ""));
    const extension = name.split(".").splice(1).join(".");
    const dlName = fillFileNameTemplate(name);
    GM_Compat.download({
      url: url,
      name: dlName,
      onload: () => {
        console.info(`Saved ${url} => ${dlName}.`);
        const downloadButton = document.querySelector("#fufo-download-state");
        downloadButton.style.color = Colors.Success;
        downloadButton.style.borderColor = Colors.Success;
        // note: this may not be true as the browser may try to uniquify an already existing file name
        downloadButton.innerText = `Saved as: ${dlName}`;
      },
      onerror: (error, details) => {
        console.error(`Failed to save ${url} => ${dlName}. Reason: ${error}. ${details}`);
        const downloadButton = document.querySelector("#fufo-download-state");
        downloadButton.innerText = "Failed. Hover for more details!";
        downloadButton.style.color = Colors.Failure;
        downloadButton.style.borderColor = Colors.Failure;
        downloadButton.style.textDecoration = "dashed underlined";
        downloadButton.innerText = `Download failed: ${getErrorDescription(error, details, extension)}`;
      },
      ontimeout: () => {
        console.error(`Failed to save ${url} => ${dlName}. Reason: Timeout`);
      },
    });
  }

  function fillFileNameTemplate(fileName) {
    const template = getFileNameTemplate();
    const filledIn = template
      .replaceAll(TemplateValues.FileName, fileName)
      .replaceAll(TemplateValues.Host, document.location.host)
      .replaceAll(TemplateValues.Path, (document.location.pathname.match(/\/.+\//) || [""])[0])
      .replaceAll(TemplateValues.DownloadTimestamp, Date.now())
      .replaceAll(/\/{2,}/g, "/"); // remove duplicate '/'
    return filledIn;
  }

  /**
   * Fetches the file name template to use for the current `document.location`.
   * Priorities are as follows:
   * 1. Domain-specific template, if configured
   * 2. General template, if defined
   * 3. Fall back to `TemplateValues.FileName`, if no other templates were configured.
   * @returns {!string}
   */
  function getFileNameTemplate() {
    const host = document.location.host;
    const domainSpecificTemplates = config
      .get(ConfigKeys.Per_Domain_File_Name_Templates)
      .split("\n");
    for (const template of domainSpecificTemplates) {
      const split = template.split(":", 2);
      if (split.length === 2 && split[0].trim() === host) {
        return split[1].trim();
      }
    }
    return config.get(ConfigKeys.File_Name_Template) || TemplateValues.FileName;
  }

  function getErrorDescription(error, details, extension) {
    switch (error.error) {
      case "not_enabled":
        return "The download feature isn't enabled for the user script.";
      case "not_whitelisted":
        return `The file extension '.${extension}'' isn't whitelisted to be downloaded via userscripts`;
      case "not_permitted":
        return `The script does not have permission to download files.`;
      case "not_supported":
        return `The download feature is not supported by the browser.`;
      case "not_succeeded":
        return details; //todo: check me.
      default:
        return `Download failed. Reason: '${error.error}'. Details: '${details}'.`;
    }
  }

  function copyToClipboard() {
    GM_Compat.setClipboard(document.location.href);
  }

  /**
   * Checks if there is a way to resize the curently loaded file to a larger/original version.:windoworigin
   * If possible, redirects to the resulting URL.
   * Also refrains from redirecting on back navigation.
   */
  function resize() {
    if (checkBackNavigation()) {
      return;
    }
    const loc = document.location;
    if (loc.host === "img.imageboss.me" && !loc.pathname.includes("/cdn/")) {
      const pathSegments = document.location.pathname.split("/").filter(x => x);
      const newHref = `${loc.origin}/${pathSegments[0]}/cdn/${pathSegments[pathSegments.length - 1]}`;
      document.location.href = newHref;
    }
  }

  /**
   * Checks if the current page load was due to a back navigation.
   * @return {!boolean} true, if it was; false otherwise.
   */
  function checkBackNavigation() {
    const performanceNavigation = window.performance.getEntriesByType("navigation")?.[0];
    if (performanceNavigation) {
      return performanceNavigation.type === "back_forward";
    } else {
      // fallback to deprecated API
      return window.performance.navigation.type === 2;
    }
  }

  function initConfig(onConfigInitialized) {
    return new GM_config({
      id: "fellomen-fufo-config",
      title: "Configuration for FOFU.user.js",
      fields: {
        [ConfigKeys.Enable_Download_Keybind]: {
          label: "Enable download keybind",
          labelPos: "right",
          title: 'Overwrites Ctrl + S keybind to save this file without a "Save as" dialog.',
          type: "checkbox",
          default: false,
        },
        [ConfigKeys.Enable_Clipboard_Keybind]: {
          label: "Enable copy to clipboard keybind",
          labelPos: "right",
          title: "Overwrites Ctrl + C keybind to copy the current URL to the clipboard.",
          type: "checkbox",
          default: false,
        },
        [ConfigKeys.Enable_Resizer]: {
          label: "Automatically navigate to the original image source",
          labelPos: "right",
          title:
            "If any image-resizing/lower res URL is found, attempts to navigate to the original size image instead.",
          type: "checkbox",
          default: false,
        },
        [ConfigKeys.Use_Override_Filenames]: {
          label: "Use override filenames",
          labelPos: "right",
          title:
            "Honors override file names (parts behind a # in the URL) which other scripts may use to define file names for downloads.",
          type: "checkbox",
          default: false,
        },
        [ConfigKeys.Show_UI]: {
          label: "Show userscript UI",
          labelPos: "right",
          type: "checkbox",
          default: true,
        },
        [ConfigKeys.File_Name_Template]: {
          label: "Default file name template:",
          labelPos: "above",
          title:
            "Available placeholders: " +
            Object.getOwnPropertyNames(TemplateValues)
              .map(x => TemplateValues[x])
              .join(", "),
          type: "text",
          default: "{filename}",
        },
        [ConfigKeys.Per_Domain_File_Name_Templates]: {
          label:
            "Per-domain file name template overrides (one per line, pattern: <domain>: <template>):",
          labelPos: "above",
          title:
            "Define templates for specific domains, will override the default file name template if applicable. Same placeholders apply.",
          type: "textarea",
        },
        Wiki_Readme: {
          section: [""], // Add an empty section to get some spacing between the settings and this button
          label: "Open Wiki/README",
          type: "button",
          click: () => {
            GM_Compat.openInTab("https://gitlab.com/fellomen/userscripts/-/wikis/fofu.user.js");
          },
        },
      },
      css: `
        * { padding: 4px; }
        .config_header {
            text-align: center;
            font-size: 24;
            padding: 8;
        }
        #fellomen-fufo-config_buttons_holder {
            padding: 40 20 10 20;
            width: 100%;
            text-align: center;
        }
        #fellomen-fufo-config_buttons_holder > * {
            display: inline-block;
            margin: 8 0;
        }
        #fellomen-fufo-config .config_var {
          margin: 0;
          padding: 0;
        }
        #fellomen-fufo-config_field_File_Name_Template {
            width: 100%;
        }
        #fellomen-fufo-config_field_Per_Domain_File_Name_Templates {
            min-height: 10em;
            width: 100%;
        }
        body {
            background: #333333EE !important;
            border: 2px solid #CEB277;
            color: #CEB277 !important;
        }
        .reset { color: #e7e9ea !important }
      `,
      events: {
        init: onConfigInitialized,
      },
    });
  }
})();
