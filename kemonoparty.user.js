// ==UserScript==
// @name         kemono-coomer
// @namespace    fellomen
// @version      1.0.1
// @author       fellomen
// @description  Improve kemono lurking
// @match        https://kemono.party/*/user/*
// @match        https://kemono.party/*/user/*/post/*
// @match        https://kemono.su/*/user/*
// @match        https://kemono.su/*/user/*/post/*
// @match        https://coomer.party/*/user/*
// @match        https://coomer.party/*/user/*/post/*
// @grant        GM_addStyle
// @grant        GM_openInTab
// @grant        GM_download
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_addValueChangeListener
// @require      https://gitlab.com/fellomen/userscripts/-/raw/download.js-1.2/tools/download.js
// @updateURL    https://gitlab.com/fellomen/userscripts/-/raw/master/kemonoparty.user.js
// @downloadURL  https://gitlab.com/fellomen/userscripts/-/raw/master/kemonoparty.user.js
// @supportURL   https://gitlab.com/fellomen/userscripts
// ==/UserScript==

(() => {
    'use strict';

    const Regex = {
        UserPage: /^https?:\/\/(?<host>.*)\/(?<paywall>.*?)\/user\/(?<id>.*?)(\?|$)/i,
        PostPage: /^https?:\/\/(?<host>.*)\/(?<paywall>.*?)\/user\/(?<userId>.*?)\/post\/(?<postId>.*)$/i
    }

    const CssClass = {
        SeenPost: "fellomen-seen-post",
        VisitedPost: "fellomen-visisted-post",
        FilesList: "fellomen-files-list",
        LinkButton: "fellomen-link-button",
        DlButton: "fellomen-dl-button",
        DlInProgress: "fellomen-dl-in-progress",
        DlSuccess: "fellomen-dl-success",
        DlFailure: "fellomen-dl-failure",
        SidebarEntry: "fellomen-sidebar-entry"
    }

    const StorageKeys = {
        DownloadTemplate: "DownloadTemplate"
    }


    if (Regex.PostPage.test(document.location)) {
        // must be before UserPage check
        handlePostPage();
    } else if (Regex.UserPage.test(document.location)) {
        handleUserPage();
    }

    function handlePostPage() {

        class PostMetaData {
            constructor(id, title, userName, userId, paywall, date, dateTime) {
                this.id = id;
                this.title = title;
                this.userName = userName;
                this.userId = userId;
                this.paywall = paywall;
                this.date = date;
                this.dateTime = dateTime;
            }

            /**
             * Returns best-effort combination of `userId` and `userName`, 
             * also handles equal values and absent values.
             * If `userName` is `undefined` or equal to `userId`, only `userId`
             * is returned; otherwise `userName (userId)` is returned
             * @returns {String}
             */
            get user() {
                if (this.userName === undefined || this.userId === this.userName) {
                    return userId;
                } else {
                    return `${this.userName} (${this.userId})`;
                }
            }
        }

        class Download {
            /**
             * @param {String} link URL for the download 
             * @param {String} name name of the file
             */
            constructor(link, name) {
                this.link = link;
                this.name = name;
            }
        }

        /**
         * Loaded from donwloader.js, see https://gitlab.com/fellomen/userscripts/-/blob/download.js-1.2/tools/download.js
         */
        const downloader = new Downloader(GM_getValue(StorageKeys.DownloadTemplate, "$fileNameWithExtension"));

        // find all downloadable content
        const downloads = [];
        document.querySelectorAll("[download]").forEach(downloadable => {
            const link = downloadable["href"];
            if (!downloads.find((d) => d.link === link)) {
                // Pages with a preview image often have the same image/link in the post body again.
                // Prevent adding multiples of the same link here.
                const fileName = decodeURIComponent(downloadable["download"]);
                downloads.push(new Download(link, fileName));
            }
        });
        // find all external links
        const externals = [];
        document.querySelectorAll(".post__content a").forEach(exeternal => {
            const link = exeternal.href;
            const name = exeternal.innerText;
            externals.push(new Download(link, name));
        });

        const metadata = getMetadata();
        const staticTempalteParameters = createStaticTemplateParameters();

        // #region UI
        GM_addStyle(`ul.${CssClass.FilesList} li.${CssClass.DlInProgress} * { color: lightyellow; }`);
        GM_addStyle(`ul.${CssClass.FilesList} li.${CssClass.DlSuccess} * { color: lightgreen; }`);
        GM_addStyle(`ul.${CssClass.FilesList} li.${CssClass.DlFailure} * { color: red; }`);
        GM_addStyle(`ul.${CssClass.FilesList} li { list-style-type: none;  }`)
        GM_addStyle(`.${CssClass.LinkButton} { cursor: pointer; padding-right: 4px; }`);

        createUserscriptUi();

        function createUserscriptUi() {
            const appendTarget = document.querySelector("div.post__body");
            const container = document.createElement("div");
            //to get the border the page is using for its post page sections
            container.style.border = "solid gray .125em";
            container.style.padding = "2vh 1vw";
            createUserScriptSection(container);
            createFileSection(container);
            createExternalSection(container);
            appendTarget.parentNode.insertBefore(container, appendTarget);

            function createUserScriptSection(container) {
                const header = document.createElement("h2");
                header.innerText = "Userscript";
                container.appendChild(header);
                const openAll = document.createElement("a");
                openAll.classList.add(CssClass.LinkButton);
                openAll.innerText = "[ Open all downloads/links in tabs ]";
                openAll.addEventListener("click", (evt) => {
                    evt.preventDefault();
                    downloads.reverse().forEach(link => GM_openInTab(link.link, true));
                    externals.reverse().forEach(link => GM_openInTab(link.link, true));
                });
                container.appendChild(openAll);
                container.appendChild(document.createElement("br"));
                container.appendChild(createDownloadTemplateUi());

                function createDownloadTemplateUi() {
                    const details = document.createElement("details");
                    const summmary = document.createElement("summary");
                    const templateInput = document.createElement("input");
                    const apply = document.createElement("button");
                    apply.innerText = "Apply"
                    apply.addEventListener("click", (evt) => {
                        GM_setValue(StorageKeys.DownloadTemplate, templateInput.value);
                        downloader.updateTemplate(templateInput.value);
                    });
                    templateInput.value = downloader.template;
                    GM_addValueChangeListener(StorageKeys.DownloadTemplate, (key, oldValue, newValue, remote) => {
                        if (remote) {
                            templateInput.value = newValue;
                            downloader.updateTemplate(newValue);
                        }
                    });
                    templateInput.style.width = "90%"
                    templateInput.style.marginLeft = "1vw";
                    summmary.innerText = "File download template";
                    details.style.fontFamily = "monospace";
                    details.innerText = `
Specify a template that is used when saving files via the [ DL ] buttons. Folders are allowed.

General placeholders:
- $userName ................ Name of the user
- $userId .................. ID of the user on the respective paywall site
- $user .................... Combines and optimizes $userName and $userId
- $postId .................. The ID of the post
- $postTitle ............... The title of this post
- $paywall ................. The paywall this post is behind
- $postDate ................ The date of this post, in the format yyyy-MM-dd
- $postDateTime ............ The date and time of this post in the format yyyyMMddhhmmss
- $host .................... The host name of the website you're currently on

File specific placeholders:
- $fileNameWithExtension ... The original file name, including extension; for example \"Image_1.png\"
- $fileName ................ The original file name, excluding extension; for example \"Image_1\"
- $fileExtension ........... The file extension of this file, including dot; for example \".png\"
`.trim();
                    details.appendChild(summmary);
                    details.appendChild(document.createElement("br"));
                    details.appendChild(document.createElement("br"));
                    details.appendChild(apply);
                    details.appendChild(templateInput);
                    return details;
                }
            }

            function createSubSectionBase(parent, headerText, listElements, additionalElements) {
                const header = document.createElement("h3");
                header.innerText = headerText;
                const list = document.createElement("ul");
                list.classList.add(CssClass.FilesList);
                listElements.forEach(li => list.appendChild(li));
                parent.appendChild(header);
                (additionalElements || []).forEach(x => parent.appendChild(x));
                parent.appendChild(list);
            }

            function createExternalSection(container) {
                const listElements = [];
                externals.forEach(external => {
                    const li = document.createElement("li");
                    const a = document.createElement("a");
                    a.href = external.link;
                    a.target = "_blank";
                    a.innerText = `(${external.name}) → ${external.link}`;
                    li.appendChild(a);
                    listElements.push(li);
                });
                createSubSectionBase(container, "External links", listElements);
            }

            function createFileSection(container) {
                const listElements = [];
                downloads.forEach(download => {
                    const li = document.createElement("li");
                    const a = document.createElement("a");
                    const dl = document.createElement("a");
                    dl.innerText = "[ DL ]";
                    dl.classList.add(CssClass.LinkButton, CssClass.DlButton);
                    dl.addEventListener("click", (evt) => {
                        evt.preventDefault();
                        // Remove previous success/failure states
                        li.classList.remove(CssClass.DlFailure, CssClass.DlSuccess);
                        const params = fillFileParameters(staticTempalteParameters, download.name);
                        li.classList.add(CssClass.DlInProgress);
                        downloader.downloadWithTemplate(download.link, params, () => {
                            li.classList.add(CssClass.DlSuccess);
                            li.classList.remove(CssClass.DlInProgress);
                        }, (e) => {
                            li.classList.add(CssClass.DlFailure);
                            li.classList.remove(CssClass.DlInProgress);
                            console.log(e);
                        });
                    });
                    a.href = download.link;
                    a.target = "_blank";
                    a.innerText = download.name;
                    li.appendChild(dl);
                    li.appendChild(a);
                    listElements.push(li);
                });
                const dlButton = document.createElement("a");
                dlButton.innerText = "[ Download all files ]";
                dlButton.classList.add(CssClass.LinkButton);
                dlButton.addEventListener("click", (evt) => {
                    downloader.enableQueue();
                    document.querySelectorAll(`.${CssClass.DlButton}`).forEach(btn => btn.click());
                    downloader.disableQueue();
                    downloader.downloadQueue();
                });
                appendTarget.appendChild(dlButton);
                createSubSectionBase(container, "Downloads", listElements, [dlButton]);
            }
        }

        // #endregion UI

        // #region metadata and templating

        function getMetadata() {
            const userName = document.querySelector(".post__user-name").innerText;
            const groups = location.href.match(Regex.PostPage).groups;
            const dateTime = document.querySelector("time.timestamp").dateTime
            const title = document.querySelector(".post__title").innerText;

            return new PostMetaData(
                groups.postId,
                title,
                userName,
                groups.userId,
                groups.paywall,
                dateTime.substring(0, dateTime.indexOf(" ")),
                dateTime.replaceAll(/[: -]/g, "")
            );
        }

        /**
         * Creates a `TemplateParameters` instance filled with all the static information of this post.
         * @param {PostMetaData} postMetaData
         */
        function createStaticTemplateParameters() {
            return new TemplateParameters()
                .addParameter("$host", document.location.host)
                .addParameter("$userName", metadata.userName)
                .addParameter("$userId", metadata.userId)
                .addParameter("$user", metadata.user)
                .addParameter("$postId", metadata.id)
                .addParameter("$postTitle", metadata.title.replaceAll(/\//g, "-"))
                .addParameter("$paywall", metadata.paywall)
                .addParameter("$postDate", metadata.date)
                .addParameter("$postDateTime", metadata.dateTime);
        }

        /**
         * Creates a copy of the passed `staticParameters`, and fills in file-specific parameters
         * @param {TemplateParameters} staticParameters TemplateParameters instance filled with static information
         * about this post.
         * @param {PostFile} file the file, which info will be inserted.
         */
        function fillFileParameters(staticParameters, fullFileName) {
            const sanitized = fullFileName.replace(/[\\\/]/g, "_");
            const split = sanitized.replace("/", "_").split(".");
            const fileName = split.shift();
            const extension = split.join(" ");

            const params = new TemplateParameters()
                .addParameter("$fileNameWithExtension", sanitized)
                .addParameter("$fileName", fileName)
                .addParameter("$fileExtension", extension);
            staticParameters.forEach((key, value) => params.addParameter(key, value));
            return params;
        }
        // #endregion metadata and templating
    }

    function handleUserPage() {

        class UserInfo {
            /**
             * @param {String} paywall which paywall website is used
             * @param {String} id unique user id (on the paywall)
             * @param {String} name display name of user
             */
            constructor(paywall, id, name) {
                this.paywall = paywall;
                this.id = id;
                this.name = name;
            }
        }
        class UserStorage {

            /**
             * @param {String[]} seenPosts array of post IDs that were already seen on the user page
             * @param {String[]} visitedPosts array of post IDs that have been actively visited
             */
            constructor(seenPosts, visitedPosts) {
                this.seenPosts = seenPosts;
                this.visitedPosts = visitedPosts;
            }
        }

        GM_addStyle(`.${CssClass.SeenPost} { border: 4px dotted orange; }`);
        GM_addStyle(`.${CssClass.VisitedPost} { border: 4px dotted green; }`);

        const user = getUserInfo();
        const storageKey = `${user.paywall}_${user.id}`;
        const storage = GM_getValue(storageKey, new UserStorage([], []));

        // check all posts on current page
        getPostsOnPage().forEach(post => {
            const postId = post.attributes["data-id"].value;
            if (storage.visitedPosts.includes(postId)) {
                post.classList.add(CssClass.VisitedPost);
            } else if (storage.seenPosts.includes(postId)) {
                post.classList.add(CssClass.SeenPost);
            } else {
                storage.seenPosts.push(postId);
            }
            post.addEventListener("mousedown", (evt) => {
                if (evt.button === 1) {
                    // only intercept middle button clicks here
                    evt.preventDefault();
                    navigateTo(post);
                }
            });
            post.addEventListener("click", evt => {
                evt.preventDefault()
                navigateTo(post);
            });

            function navigateTo(post) {
                post.classList.remove(CssClass.SeenPost);
                post.classList.add(CssClass.VisitedPost);
                if (!storage.visitedPosts.includes(postId)) {
                    storage.visitedPosts.push(postId);
                    updateUserStorage();
                }
                const link = post.querySelector("a").href;
                GM_openInTab(link, true);
            }
    
        });
        updateUserStorage();
        addSidebarEntry();

        function addSidebarEntry() {
            const group = createSidebarGroup();

            GM_addStyle(`.${CssClass.SidebarEntry} span { text-align: center; }`);

            const seenTotal = document.createElement("span");

            const paginationText = document.querySelector("#paginator-top > small").innerText;
            const totalPosts = paginationText.match(/.* of (?<totalPosts>\d+)/).groups.totalPosts;
            seenTotal.innerText = `Seen: ${storage.seenPosts.length}/${totalPosts}`;

            const resetSeenHeader = document.createElement("span");
            resetSeenHeader.innerText = "Reset seen ..."

            group.appendChild(seenTotal);
            group.appendChild(resetSeenHeader);
            group.appendChild(createButton("on current page", (evt) => {
                getPostsOnPage().forEach(post => {
                    post.classList.remove(CssClass.SeenPost);
                    const postId = post.attributes["data-id"].value;
                    storage.seenPosts = storage.seenPosts.filter(x => x !== postId);
                });
                updateUserStorage();
            }));
            group.appendChild(createButton("new only", (evt) => {
                getPostsOnPage().forEach(post => {
                    if (!post.classList.contains(CssClass.SeenPost)) {
                        const postId = post.attributes["data-id"].value;
                        const index = storage.seenPosts.indexOf(postId);
                        storage.seenPosts = storage.seenPosts.filter(x => x !== postId);
                    }
                });
                updateUserStorage();
            }));
            group.appendChild(createButton("all pages", (evt) => {
                getPostsOnPage().forEach(post => {
                    post.classList.remove(CssClass.SeenPost);
                });
                storage.seenPosts = [];
                updateUserStorage();
            }));
        }

        function getPostsOnPage() {
            return document.querySelectorAll("article.post-card");
        }

        function createButton(text, onClick) {
            const button = document.createElement("button");
            button.innerText = text;
            button.addEventListener("click", onClick);
            return button;
        }


        function getUserInfo() {
            const match = document.location.href.match(Regex.UserPage);
            const nameSpan = document.querySelector("h1.user-header__name span[itemprop='name']");
            return new UserInfo(
                match.groups.paywall,
                match.groups.id,
                nameSpan.innerText
            );
        }

        function updateUserStorage() {
            GM_setValue(storageKey, storage);
        }
    }

    /**
     * Creates a sidebar entry for the userscript and returns the div that each
     * individual page can insert it's own content into.
     * @returns {Element} the div to insert your content into
     */
    function createSidebarGroup() {
        const sidebar = document.querySelector("div.global-sidebar");
        const stuckBottom = sidebar.querySelector(".stuck-bottom");
        const group = document.createElement("div");
        group.classList.add("global-sidebar-entry", CssClass.SidebarEntry);

        const header = document.createElement("div");
        header.classList.add("global-sidebar-entry-item", "header");
        header.innerText = "Userscript";

        group.appendChild(header);

        sidebar.insertBefore(group, stuckBottom);

        return group;
    }
})();