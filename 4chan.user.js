// ==UserScript==
// @name         Conga: 4channel
// @namespace    fellomen
// @version      0.0.2
// @description  Improved 4chinz lurking (builds on top of 4chanx)
// @author       fellomen
// @match        https://boards.4chan.org/*/thread/*
// @grant        GM_download
// @grant        GM.download
// @grant        GM_registerMenuCommand
// @grant        GM.registerMenuCommand
// @grant        GM_info
// @grant        GM.info
// @grant        GM_setClipboard
// @grant        GM.setClipboard
// @require      https://gitlab.com/fellomen/userscripts/-/raw/compat.js-2.0.0/tools/compat.js
// @require      https://gitlab.com/fellomen/userscripts/-/raw/download.js-2.0.1/tools/download.js
// @require      https://gitlab.com/fellomen/userscripts/-/raw/congalala.js-1.1.0/tools/congalala.js
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @supportURL   https://gitlab.com/fellomen/userscripts
// @homepageURL  https://gitlab.com/fellomen/userscripts
// ==/UserScript==

(() => {
  "use strict";

  const Settings = {
    EnableSaveThread: "Enable_Save_Thread",
    EnableGalleryNavigation: "Enable_Gallery_Navigation",
    EnableGallerySwiping: "Enable_Gallery_Swiping",
    EnableExternalLinkList: "Enable_External_Link_List",
  };

  const Placeholders = {
    BoardAbbreviation: "{boardabbr}",
    BoardTitle: "{boardtitle}",
    ThreadTitle: "{threadtitle}",
    ThreadDate: "{threaddate}",
    FileTimestamp: "{filetimestamp}",
  };

  class ThreadMetadata {
    /**
     *
     * @param {String} boardAbbreviation board abbreviation, ie.: "a", "vrpg"
     * @param {String} boardTitle full board title, ie.: "Anime & Manga", "Video Games RPG"
     * @param {String} threadTitle title of the thread
     * @param {String} threadDate UTC timestamp of the OP post
     */
    constructor(boardAbbreviation, boardTitle, threadTitle, threadDate) {
      this.boardAbbreviation = boardAbbreviation;
      this.boardTitle = boardTitle;
      this.threadTitle = threadTitle;
      this.threadDate = threadDate;
    }
  }

  const threadMetaData = getThreadMetadata();
  const defaultTemplate = buildDefaultTemplate();

  const conga = new Congalala(
    () => {
      const isInGalleryView = document.querySelector(".gal-image") !== null;
      if (isInGalleryView) {
        return [];
      }
      const dlTimestamp = Date.now();
      const files = [...document.querySelectorAll("span.file-info > a:first-child")].map(file => {
        return { src: file.href, parameters: fillTemplateParameters(file, dlTimestamp) };
      });
      return files;
    },
    defaultTemplate,
    Congalala.getPlaceholderValues(Placeholders),
  );
  conga.init(() => {
    if (!document.documentElement.classList.contains("fourchan-x")) {
      console.warn(
        "This userscript is built with the assumption of 4chanX being installed. Some features may not work.",
      );
      return;
    }
    if (conga.config.get(Settings.EnableGallerySwiping)) {
      addSwipeNavigationTo4chanxGallery();
    }
    if (conga.config.get(Settings.EnableGalleryNavigation)) {
      addGalleryNavigation();
    }
    if (conga.config.get(Settings.EnableExternalLinkList)) {
      addExternalLinkList();
    }
  }, buildScriptSpecificSettings());

  function buildDefaultTemplate() {
    const boardDirectory = `4chan/${Placeholders.BoardAbbreviation}`;
    const threadDirectory = `${Placeholders.ThreadTitle} (${Placeholders.ThreadDate})`;
    const file = `${Congalala.DEFAULT_PLACEHOLDERS.FileName}.${Congalala.DEFAULT_PLACEHOLDERS.Extension}`;
    return boardDirectory + "/" + threadDirectory + "/" + file;
  }

  /**
   * Create extra GM_config fields for passing into {@link Congalala}.
   * @returns {!object} the extra fields.
   */
  function buildScriptSpecificSettings() {
    return {
      [Congalala.ConfigKeys.EnableDownloadKeybind]: {
        section: ["Script settings"],
        label: "Enable thread batch downloading",
        labelPos: "right",
        title:
          "Overwrites Ctrl + S keybind to save all files in the thread, while not in the Gallery view.",
        type: "checkbox",
        default: true,
      },
      [Settings.EnableGalleryNavigation]: {
        label:
          "Enable gallery navigation: A/D to navigate through the gallery." +
          "S to download the current file. " +
          "If there is a Pixiv sauce link available, this will be opened instead.",
        labelPos: "right",
        title: "Enable gallery navigation keybinds.",
        type: "checkbox",
        default: true,
      },
      [Settings.EnableGallerySwiping]: {
        label: "Enable swipe gesture navigations on the gallery image.",
        labelPos: "right",
        title: "",
        type: "checkbox",
        default: true,
      },
      [Settings.EnableExternalLinkList]: {
        label: "Collect and display all links to external sites at the bottom of the page.",
        labelPos: "right",
        title: "",
        type: "checkbox",
        default: true,
      },
    };
  }

  /**
   * Creates a {@link TemplateParameters} instance filled with all available metadata from the
   * file (parsed from the `a` parameter) and {@link #threadMetaData}.
   * Supports anchor elements from the 4chan-x gallery as well as normal thread view.
   * @param {!HTMLAnchorElement} a the anchor to parse from.
   * @param {?String} dlTimestamp timestamp to use as value for {timestamp} template, falls back to
   *                               {@link Date.now()} if not supplied.
   */
  function fillTemplateParameters(a, dlTimestamp) {
    // 4chan-x gallery uses download on the anchor
    // 4chan-x may also abbreviate long file names, a child span with class fnfull will contain
    // the full file name
    const ogFullFileName = a.download || a.querySelector("span.fnfull")?.innerText || a.innerText;
    const split = ogFullFileName.split(".");
    const extension = split.pop();
    const fileName = split.join(".");
    const fileTimestamp = a.href.split("/").pop().split(".")[0];
    const parameters = Congalala.createDefaultTemplateParameters(a, dlTimestamp || Date.now())
      .addParameter(Congalala.DEFAULT_PLACEHOLDERS.FileName, fileName)
      .addParameter(Placeholders.BoardAbbreviation, threadMetaData.boardAbbreviation)
      .addParameter(Placeholders.BoardTitle, threadMetaData.boardTitle)
      .addParameter(Placeholders.ThreadTitle, threadMetaData.threadTitle)
      .addParameter(Placeholders.ThreadDate, threadMetaData.threadDate)
      .addParameter(Placeholders.FileTimestamp, fileTimestamp);
    return parameters;
  }

  /**
   * Parses metadata of the current thread.
   * @return {!ThreadMetadata}
   */
  function getThreadMetadata() {
    const titleRegex =
      /(?<unread_post_count>\(\d+\) )?\/(?<board>.+?)\/ - (?<thread_title>.*) - (?<board_name>.*) - 4chan/i;
    const titleGroups = document.title.match(titleRegex).groups;
    return new ThreadMetadata(
      titleGroups.board,
      titleGroups.board_name,
      titleGroups.thread_title.replace(/[\/\\\.]/g, "_"),
      document.querySelector("div.post.op span.dateTime").getAttribute("data-utc"),
    );
  }

  function addGalleryNavigation() {
    const downloadCache = [];

    Congalala.createAddedNodesObserver(
      () => {
        document.body.addEventListener("keydown", handleGalleryKeybinds);
        document.body.addEventListener("wheel", handleGalleryMousewheel);
        downloadCache.forEach(downloaded => {
          const match = document.querySelector(`#a-gallery .gal-thumb[href="${downloaded}"]`);
          if (match) {
            match.style.border = "2px solid " + Congalala.COLORS.Success;
          }
        });
      },
      node => node.id === "a-gallery",
    ).observe(document.body, { childList: true, subtree: true });
    Congalala.createdRemovedNodesObserver(
      () => {
        document.body.removeEventListener("keydown", handleGalleryKeybinds);
        document.body.removeEventListener("wheel", handleGalleryMousewheel);
      },
      node => node.id === "a-gallery",
    ).observe(document.body, { childList: true, subtree: true });

    function handleGalleryKeybinds(evt) {
      switch (evt.code) {
        case "KeyS":
        case "ArrowDown":
          const sauce = document.querySelector(".gal-sauce > a");
          if (sauce && sauce.innerText === "pixiv") {
            // open on pixiv if sauce is available
            GM_Compat.openInTab(sauce.href);
            return;
          }
          downloadCurrentImage();
          copyCurrentGalleryImageUrlToClipboard();
          break;
        case "KeyA":
          document.querySelector(".gal-prev").click();
          break;
        case "KeyC":
          copyCurrentGalleryImageUrlToClipboard();
          break;
        case "KeyD":
          document.querySelector(".gal-next").click();
          break;
      }
    }

    function downloadCurrentImage() {
      const galName = document.querySelector("a.gal-name");
      const href = galName.href;
      const parameters = fillTemplateParameters(galName);
      const template = conga.config.get(Congalala.ConfigKeys.FileNameTemplate, defaultTemplate);
      new Downloader(template).downloadWithTemplate(
        href,
        parameters,
        () => {
          // onSuccess
          downloadCache.push(href); // could lead to duplicates, but also does not really matter
          const thumb = document.querySelector(`#a-gallery a.gal-thumb[href='${href}']`);
          if (thumb) {
            thumb.style.border = "2px solid " + Congalala.COLORS.Success;
          }
        },
        () => {
          // onFailure
          const thumb = document.querySelector(`#a-gallery a.gal-thumb[href='${href}']`);
          if (thumb) {
            thumb.style.border = "2px solid " + Congalala.COLORS.Failure;
          }
        },
      );
    }

    function copyCurrentGalleryImageUrlToClipboard() {
      if (true) {
        // todo: add config check here
        GM_Compat.setClipboard(document.querySelector(".gal-name").href);
      }
    }

    function handleGalleryMousewheel(evt) {
      if (evt.deltaY > 0) {
        document.querySelector(".gal-next").click();
      } else if (evt.deltaY < 0) {
        document.querySelector(".gal-prev").click();
      }
    }
  }

  function addSwipeNavigationTo4chanxGallery() {
    let touchStartX = 0;
    let touchEndX = 0;
    Congalala.createAddedNodesObserver(
      node => {
        const galleryImage = node.querySelector("div.gal-image");
        galleryImage.addEventListener("pointerdown", evt => {
          evt.preventDefault();
          touchStartX = evt.clientX;
          galleryImage.setPointerCapture(evt.pointerId);
        });
        galleryImage.addEventListener("pointerup", evt => {
          evt.preventDefault();
          touchEndX = evt.clientX;
          galleryImage.releasePointerCapture(evt.pointerId);
          handleSwipe();
        });
      },
      node => node instanceof HTMLDivElement && node.querySelector("div.gal-image"),
    ).observe(document.body, { childList: true, subtree: true });

    function handleSwipe() {
      if (touchStartX < touchEndX) {
        document.querySelector(".gal-prev").click();
      } else if (touchEndX > touchStartX) {
        document.querySelector(".gal-next").click();
      }
      touchStartX = 0;
      touchEndX = 0;
    }
  }

  /**
   * @todo needs some better UI/UX:
   * - Better styling
   * - Add backlings to the post(s) the link was in.
   */
  function addExternalLinkList() {
    const anchorCache = [];
    const container = document.createElement("div");
    container.style.padding = "4px";
    const header = document.createElement("h4");
    header.innerText = "Links posted in this thread:";
    container.appendChild(header);
    document.body.appendChild(container);
    document.querySelectorAll("a.linkify").forEach(a => addLinkToContainer(a));

    Congalala.createAddedNodesObserver(
      node => addLinkToContainer(node),
      node => isExternalLink(node) && !anchorCache.includes(node),
    ).observe(document.querySelector(".board"), { childList: true, subtree: true });

    /**
     * Whether a given {@link HTMLElement} is a link to an extenral site.
     * @param {!HTMLElement} node - the element to check
     * @returns {!boolean}
     */
    function isExternalLink(node) {
      return node instanceof HTMLAnchorElement && node.classList.contains("linkify");
    }

    /**
     * Adds the given link to the container, if no link with the same href is inside it yet.
     * @param {HTMLAnchorElement} a - the link to add
     */
    function addLinkToContainer(a) {
      anchorCache.push(a);
      if (container.querySelector(`a[href="${a.href}]`) === null) {
        container.appendChild(a.cloneNode(true));
        container.appendChild(document.createElement("br"));
      }
    }
  }
})();
