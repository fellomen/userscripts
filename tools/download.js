/**
 * Util class to simplify downloading via userscript and supporting template filenames.
 * Now with support for optional whitespace depending on presence of parameter.
 * See {@link TemplateParameters} for more information.
 * Forked from: https://bitbucket.org/Rhiki/rhikis-userscripts/src/download.js-1.1/tools/download.js
 * Unless you manually supply a gmDownloadFn at the constructor, the GM_Compat >= 1.5.0, ie.:
 * // (at)require  https://gitlab.com/fellomen/userscripts/-/raw/compat.js-1.5.0/tools/compat.js
 */
class Downloader {
  static get VERSION() {
    return "2.0.1";
  }

  /**
   * @param {!string} template Download name template to use for download operations.
   * @param {(function(object):void|undefined)} gmDownloadFn A GM function to use for downloading,
   * either GM_download, GM.download, GM_Compat.download.
   * See https://violentmonkey.github.io/api/gm/#gm_download
   * If not supplied, will fall back to GM_Compat.download.
   */
  constructor(template, gmDownloadFn) {
    this.template = template;
    this.gmDownloadFn = gmDownloadFn || GM_Compat.download;
    this.queue = [];
    this.enqueue = false;
  }

  enableQueue() {
    this.enqueue = true;
  }

  disableQueue() {
    this.enqueue = false;
  }

  /**
   * Updates the file name template for future downloads. Note that currently enqueud downloads
   * are not affected by this change.
   * @param {String} template the new filename tempalte to use
   */
  updateTemplate(template) {
    this.template = template;
  }

  /**
   * Downloads a given `src` to `fileName`.
   * @param {string} src the file to download
   * @param {string} fileName the name as which the file should be saved
   * @param {function():void} onSuccess callback on successful download; optional
   * @param {function(string):void} onFailure callback on failed download, contains an error message; optional
   * @param {string} referer string to be used as referer; optional, if `undefined` the header will not be set
   * detailing the error; optional
   */
  download(src, fileName, onSuccess, onFailure, referer) {
    this.#download(src, fileName, onSuccess, onFailure, referer);
  }

  /**
   * Downloads a given `src` and downloads it to a file name that will be created from `this.template`
   * and parameters from `fileNameParamters`.
   * @param {string} src the file to download
   * @param {TemplateParameters} fileNameParameters the name as which the file should be saved
   * @param {function():void} onSuccess callback on successful download; optional
   * @param {function(string):void} onFailure callback on failed download, contains an error message; optional
   * @param {string} referer string to be used as referer; optional, if `undefined` the header will not be set
   * detailing the error; optional
   */
  downloadWithTemplate(src, fileNameParameters, onSuccess, onFailure, referer) {
    this.#download(src, this.#fillTemplate(fileNameParameters), onSuccess, onFailure, referer);
  }

  /**
   * Implements the actual download logic.
   * @param {string} src the file to download
   * @param {string} fileName the name as which the file should be saved
   * @param {function():void} onSuccess callback on successful download; optional
   * @param {function(string):void} onFailure callback on failed download, contains an error message; optional
   * @param {string} referrer string to be used as referer; optional, if `undefined` the header will not be set
   * detailing the error; optional
   */
  #download(src, fileName, onSuccess, onFailure, referrer) {
    const downloadDetails = {
      url: src,
      name: fileName,
    };
    if (onSuccess !== undefined) {
      downloadDetails.onload = () => onSuccess();
    }
    if (onFailure !== undefined) {
      downloadDetails.onerror = error =>
        onFailure(`Download failed for: ${src} => ${fileName}. Reason: ${error.error}`);
      downloadDetails.ontimeout = () => onFailure(`Download timed out for: ${src}.`);
    }
    if (referrer !== undefined) {
      downloadDetails.headers = { referer: referrer };
    }
    if (this.enqueue) {
      this.queue.push(downloadDetails);
    } else {
      this.gmDownloadFn(downloadDetails);
    }
  }

  downloadQueue() {
    this.#downloadQueue();
  }

  abortQueue() {
    this.queue = [];
  }

  #downloadQueue() {
    const details = this.queue.shift();
    if (details !== undefined) {
      interceptCallback(details, "onload", () => this.#downloadQueue());
      interceptCallback(details, "onerror", () => this.#downloadQueue());
      interceptCallback(details, "ontimeout", () => this.#downloadQueue());
      this.gmDownloadFn(details);
    }

    function interceptCallback(object, callbackName, continuation) {
      const temp = object[callbackName];
      // onerror callback uses arg1 so we have to pass it here,
      // it shouldn't interfer with the other callbacks.
      object[callbackName] = arg1 => {
        temp(arg1);
        continuation();
      };
    }
  }

  /**
   * Replaces placeholders in `this.template` with the parameters provided by the
   * `parameters` argument.
   * @param {!TemplateParameters} parameters
   * @returns {!string} the resulting file name
   */
  #fillTemplate(parameters) {
    let fileName = this.template;
    parameters.forEach((placeHolder, value) => {
      if (placeHolder instanceof RegExp) {
        const matches = fileName.matchAll(placeHolder).toArray().reverse();
        for (const match of matches) {
          const replacement = match.groups["prefix"] + value + match.groups["suffix"];
          const adjusted = replacement.trim().length === 0 ? "" : replacement;
          const preMatch = fileName.substring(0, match.index);
          const afterMatch = fileName.substring(match.index + match[0].length);
          fileName = preMatch + adjusted + afterMatch;
        }
      } else {
        fileName = fileName.replaceAll(placeHolder, value);
      }
    });
    return fileName;
  }
}

/**
 * Container for key:value paramters to fill into file name templates.
 *
 * If a key is surrounded by curly brackets (ie `{filename}`) it will use RegExp
 * matching to find its usages and you may add optional whitespace prefix or suffix
 * to the placeholder. The extra whitespace is then only used, if the placeholder
 * evaluates to a non-blank string.
 */
class TemplateParameters {
  /**
   * RegEx to match against whitespace-supporting template parameters.
   * Examples of matching:
   * | input                | prefix | key        | suffix |
   * | -------------------- | ------ | ---------- | ------ |
   * | "{filename}"         | <none> | "filename" | <none> |
   * | "{ filename}"        | " "    | "filename" | <none> |
   * | "{    filename}"     | "    " | "filename" | <none> |
   * | "{ filename }"       | "  "   | "filename" | " "    |
   * | "{    filename    }" | "    " | "filename" | "    " |
   * | "{filename }"        | <none> | "filename" | " "    |
   * | "{filename    }"     | <none> | "filename" | "    " |
   */
  static #parameterRegex = /{(?<optional>\??)(?<prefix> *)(?<key>.*?)(?<suffix> *)}/;

  constructor() {
    /**
     * The defined key:value pairs.
     * @type {TemplateParameter}
     */
    this.values = [];
  }

  /**
   * Add a given placeHolder:value pair to this instance.
   * If the given placeHolder is already defined, it will be overwritten.
   * If `placeHolder` or `value` are undefined, it will not be added to
   * this container.
   * @param {!String} placeHolder - the key for the template parameter,
   * undefined input will be skipped.
   * @param {?any} value - the value to fill in for the parameter.
   * Undefined input will be replaced with the empty string.
   * If the parameter is not a string, it will be converted to a string.
   */
  addParameter(placeHolder, value) {
    if (!placeHolder) {
      return this;
    }
    value = value || "";
    if (typeof value !== "string") {
      value = value.toString();
    }
    const existing = this.values.find(x => x.placeHolder === placeHolder);
    if (existing) {
      existing.value = value;
    } else {
      this.values.push({ placeHolder: placeHolder, value: value });
    }
    return this;
  }

  /**
   * Iterates over all template parameters of this object.
   * @param {TemplateParametersForEachCallback} callback
   */
  forEach(callback) {
    for (let i = 0; i < this.values.length; i++) {
      const parameter = this.values[i];
      const match = parameter.placeHolder.match(TemplateParameters.#parameterRegex);
      if (!match) {
        // parameter does not use the {key} syntax, treat it as string
        callback(parameter.placeHolder, parameter.value);
        continue;
      }
      const placeholder = TemplateParameters.#createParameterMatcher(match.groups["key"]);
      const value = match.groups["prefix"] + parameter.value + match.groups["suffix"];
      callback(placeholder, value);
    }
  }

  /** Creates a {@link RegExp} instance, that can be passed into the {@link Downloader}
   * to allow with replacing optional whitespace intact.
   * @param {!String} placeHolderName - the name of the placeholder
   * @returns {!RegExp} the created regex matcher
   */
  static #createParameterMatcher(placeHolderName) {
    const toModify = TemplateParameters.#parameterRegex.source;
    const modified = toModify.replace(".*?", placeHolderName);
    return new RegExp(modified, "g");
  }
}

/**
 * Called for each parameter in this container.
 * @callback TemplateParametersForEachCallback
 * @param {!String|!RegeXp} placeHolder - either a string to directly replace or
 * a regex to match against for replacement
 * @param {!String} value - the value to fill the placeholder with
 * parameter from the string.
 * @returns {void}
 */

/**
 * Key:Value pair for tempalte parameters
 * @typedef TemplateParameter
 * @property {!String} placeHolder - the key for the template parameter
 * @property {?String} value - the value to fill in for the given placeHoler key
 */
