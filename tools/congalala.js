/**
 * # Congalala
 * A small framework to help with building userscripts that mainly focus on batch downloading
 * files from a site.
 *
 * # Features
 * - Comes with a default configuration for GM_config:
 *   - Enable batch download keybind (Ctrl + S, default on)
 *   - Configurable download name template (provided via `defaultFileNameTemplate`)
 *   - Developer mode (default off, adds logging and dry-runs downloads)
 *   - Adds an about section, which displays script/environment information
 * - Queued batch downloading for any files supplied via `collectFiles` parameter
 * - Download name template can be extended as needed via `templatePlaceholders` parameter
 * - Placeholders can have included whitespace like: {myparamter }, which then would fill
 *   in like "myvalue ", if the paramter is present or skips the entire parameter including
 *   white space, if it is not provided.
 * - Configuration for GM_config can be entirely overwritten by providing your own config via
 *     `gmConfigJson` parameter or alternativel you can modify the default configuration by
 *     passing a GM_config JSON object into the init()-call.
 * - You can additionally support multiple setting profiles by supplying a the configInstance
 *     parameter in the init()-call.
 * - Adds a UI showing the current download progress, when a download is started.download
 * - Adds Ctrl + C keybind to abort the current download progress.
 *
 * # Usage
 * ```js
 * const collectFiles = (() => {
 *    const files = [...document.querySelectorAll("a")].map(a => ({
 *        src: a.href,
 *        parameters: Congalala.createDefaultTemplateParameters(a),
 *     }));
 *     return files;
 * });
 * const congalala = new Congalala(collectFiles).init(() => {
 *   console.log("Initialization finished");
 * });
 * ```
 *
 * # Requirements
 * - The following @grant are needed:
 *   - GM_download / GM.download -> Required for actual downloading, can be skipped if you are doing
 *                                  some weird hackery (Downloading can be skipped if your
 *                                  collectFiles returns an empty array).
 *   - GM_registerMenuCommand / GM.registerMenuCommand -> Required, used to add script settings.
 *   - GM_info / GM.info -> Required, used to gather script/environment information for display.
 *   - GM.getValue / GM_getValue -> Can be skipped if your script is only running on one domain.
 *   - GM.setValue / GM_setValue -> Can be skipped if your script is only running on one domain.
 * - The following @require are needed:
 *   - https://gitlab.com/fellomen/userscripts/-/raw/compat.js-1.5.0/tools/compat.js (Version 2.0.0 or higher)
 *   - https://gitlab.com/fellomen/userscripts/-/raw/download.js-2.0.0/tools/download.js (Version 2.0.0 or higher)
 *   - https://openuserjs.org/src/libs/sizzle/GM_config.js
 */
class Congalala {
  /**
   * Version of Congalala.
   */
  static get VERSION() {
    return "1.1.0";
  }

  /**
   * Keys used by Congalala in GM_config.
   * @todo Add a way for implementing scripts to disable/add configurations without having to
   *       to completely replace the configuration via {@link Congalala.#gmConfigJson}.
   */
  static ConfigKeys = {
    EnableDownloadKeybind: "Enable_Download_Keybind",
    FileNameTemplate: "File_Name_Template",
    DebugMode: "Debug_Mode",
  };

  /**
   * Colors used in UI elements created by Congalala.
   */
  static COLORS = {
    Foreground: "#CEB277",
    Background: "#333333",
    Success: "green",
    Failure: "red",
  };

  /**
   * Template placeholders that will be supplied by Congalala by default.
   * @type {!string[]}
   */
  static DEFAULT_PLACEHOLDERS = {
    FileName: "{filename}",
    Host: "{host}",
    Path: "{path}",
    DownloadTimestamp: "{dl_timestamp}",
    Extension: "{ext}",
  };

  /**
   * Fallback value for file name templates, if not supplied by implementing script.
   * @type {!string}
   */
  static #DEFAULT_TEMPLATE = Congalala.DEFAULT_PLACEHOLDERS.FileName;

  /**
   * Stores the resulting config object of {@link GM_config#init}. Will be set when calling
   * {@link #init}.
   * @type {?GM_config}
   */
  #config = undefined;

  /**
   * Holds all placeholder values that may be filled in when downloading a file.
   * Filled in the constructor.
   * @type {!string[]}
   * See also: {@link #DEFAULT_PLACEHOLDERS}
   */
  #placeholders = undefined;

  /**
   * Defines the default template for file downloads. Will be used in GM_config and as fallback
   * value.
   * @type {!string}
   * See also: {@link #DEFAULT_TEMPLATE}
   */
  #defaultTemplate = undefined;

  /**
   * Returns an array of files to download.
   * @type {!function():!DownloadInfo[]}
   */
  #collectFiles = undefined;

  /**
   * If set, used to override the configuration supplied into {@link GM_Config} constructor.
   * @type {?string}
   */
  #gmConfigJson = undefined;

  /**
   * Creates a Congalala object. Call {@link #init} before using it.
   * @param {function():!DownloadInfo[]} collectFiles Callback to gather all files to be downloaded
   *                                                  on the current page
   * @param {?string} defaultFileNameTemplate the file name template to set and use as default value
   *                                          in GM_config. Does **not** override user defined
   *                                          templates in GM_config.
   * @param {?string[]} templatePlaceholders additional placeholders. Note these are only used for
   *                                         display in GM_config. You will still have to populate
   *                                         the parameters yourself.
   * @param {?string} gmConfigJson Can be supplied to completely override the configuration for
   *                               GM_config.
   */
  constructor(collectFiles, defaultFileNameTemplate, templatePlaceholders, gmConfigJson) {
    this.#defaultTemplate = defaultFileNameTemplate || Congalala.#DEFAULT_TEMPLATE;
    this.#collectFiles = collectFiles || [];
    this.#gmConfigJson = gmConfigJson;
    this.#placeholders = Congalala.getPlaceholderValues(Congalala.DEFAULT_PLACEHOLDERS).concat(
      templatePlaceholders || [],
    );
  }

  get placeholders() {
    return this.#placeholders;
  }
  get config() {
    return this.#config;
  }

  /**
   * Perform necessary initializations.
   * @param {!function():void} onInitializationComplete Callback when initialization is finished
   * @param {?Object} fields Allows you to specify extra fields for GM_config to be added.
   *                          See https://github.com/sizzlemctwizzle/GM_config/wiki#getting-started
   *                          for how to write these.
   * @param {?string} configInstance Specify the ID for the configuration to load. Only necessary,
   *                                 if you want to have multiple profiles. If left undefined, it
   *                                 will simply create a default instance
   * @return {!Congalala} this instance for chaining calls
   */
  init(onInitializationComplete, customFields, configInstance) {
    this.#config = this.#initConfig(
      () => {
        GM_Compat.registerMenuCommand(`Script settings`, () => this.#config.open(), "p");
        if (this.#config.get(Congalala.ConfigKeys.DebugMode)) {
          const info = GM_Compat.info();
          console.warn(info.script.name + " is running in debug mode. Downloads may not work.");
          console.debug(this.#buildScriptInfoText());
          console.debug(this.#buildRuntimeInfoText());
          console.debug(info);
        }
        document.addEventListener("keydown", evt => {
          if (
            evt.code === "KeyS" &&
            evt.ctrlKey &&
            this.#config.get(Congalala.ConfigKeys.EnableDownloadKeybind)
          ) {
            evt.preventDefault();
            const downloadItems = this.#collectFiles();
            if (downloadItems && downloadItems.length > 0) {
              this.#startQueuedDownload(downloadItems);
            }
          }
        });
        document.addEventListener("keydown", evt => {
          if (evt.code === "KeyC" && evt.ctrlKey && this.#downloadProgress.enqueued > 0) {
            evt.preventDefault();
            this.#downloadProgress.downloader.abortQueue();
            this.#downloadProgress.enqueued = 0;
          }
        });
        onInitializationComplete?.call();
      },
      customFields || {},
      configInstance,
    );
    return this;
  }

  /**
   * Convert a key-value map to an array of values.
   * @param {object} object Object with key-value pairs for placeholders
   * @return {!string[]} An array of all placeholders defined in `object`
   */
  static getPlaceholderValues(object) {
    return Object.getOwnPropertyNames(object).map(x => object[x]);
  }

  /* ================================ GM_config stuff goes here ================================ */

  /**
   * Initializes GM_Config.
   * @param {!function():void} onConfigInitialized Callback to execute once GM_config has finished
   *                                               initialization
   * @param {?Object} fields Allows you to specify extra fields for GM_config to be added.
   *                          See https://github.com/sizzlemctwizzle/GM_config/wiki#getting-started
   *                          for how to write these.
   * @param {string} configInstance Specify the ID for the configuration to load. Only necessary,
   *                                if you want to have multiple profiles. If left undefined, it
   *                                will simply create a default instance
   * @return {!object} the config object returned by GM_config, can be used to (ie) access #get(key)
   */
  #initConfig(onConfigInitialized, customFields, configInstance) {
    const defaultSettingsScript = {
      [Congalala.ConfigKeys.EnableDownloadKeybind]: {
        section: ["Script settings"],
        label: "Enable download keybind",
        labelPos: "right",
        title: 'Overwrites Ctrl + S keybind to save this file without a "Save as" dialog.',
        type: "checkbox",
        default: true,
      },
      [Congalala.ConfigKeys.FileNameTemplate]: {
        label: "Download file name template:",
        labelPos: "above",
        title: "Available placeholders: " + this.#placeholders.join(", "),
        type: "text",
        default: this.#defaultTemplate,
      },
    };

    const defaultSettingsDeveloper = {
      About_ScriptInfo: {
        section: ["About"],
        label: this.#buildScriptInfoText(),
        type: "hidden",
      },
      About_RuntimeInfo: {
        label: this.#buildRuntimeInfoText(),
        type: "hidden",
      },
      [Congalala.ConfigKeys.DebugMode]: {
        section: ["Developer"],
        label: "Enable debug mode",
        labelPos: "right",
        title: "Enable log outputs for debugging. May also stop downloads from actually running.",
        type: "checkbox",
        default: false,
      },
    };

    const merged = { ...defaultSettingsScript, ...customFields, ...defaultSettingsDeveloper };

    return new GM_config(
      this.#gmConfigJson || {
        id: configInstance || "default-config",
        title: "Configuration for " + GM_Compat.info().script.name,
        fields: merged,
        css: `
        * {
          padding: 4px;
          font-family: jetbrains mono, monospace !important;
        }
        .config_header {
            text-align: center;
            font-size: 24pt;
            padding: 8;
        }
        [id$='config_buttons_holder'] {
            padding: 40 20 10 20;
            width: 90%;
            text-align: center !important;
        }
        [id$='config_buttons_holder'] > * {
            display: inline-block;
            margin: 8 0;
        }
        .config_var {
          margin: 0;
          padding: 0;
        }
        .config_var input[type='text'] {
            width: 100%;
        }
        .section_header {
          border: none !important;
          border-top: 2px dotted ${Congalala.COLORS.Foreground} !important;
          text-align: left !important;
          font-size: 16pt !important;
          margin-bottom: 8px !important;
        }
        body {
            background: ${Congalala.COLORS.Background} !important;
            border: 2px solid ${Congalala.COLORS.Foreground};
            color: ${Congalala.COLORS.Foreground} !important;
        }
        .reset { color: #e7e9ea !important }
      `,
        events: {
          init: onConfigInitialized,
        },
      },
    );
  }

  /**
   * Gathers information about the currently running userscript.
   * @returns {!string} A formatted string containing all gathered information.
   */
  #buildScriptInfoText() {
    const info = GM_Compat.info();
    const script = `${info.script.name} (Version: ${info.script.version}) by ${info.script.author}`;
    const updates = `, updates enabled: ${info.scriptWillUpdate}`;
    const congalala = `, Congalala version: ${Congalala.VERSION}`;
    return script + updates + congalala;
  }

  /**
   * Gathers information about the environment the current script is running in.
   * Available information differ depending on the used userscript extension.
   * @returns {!string} A formatted string containing all gathered information.
   * @todo Expand the switch-cases for other userscript extensions.
   */
  #buildRuntimeInfoText() {
    const info = GM_Compat.info();
    // AFAIK so far only supported in Tampermonkey
    // For Violentmonkey support, see: https://github.com/violentmonkey/violentmonkey/issues/2094
    const directoryInFileNamesAllowed = info.downloadMode === "native";
    const [browser, browserVersion, os, mobile] = (() => {
      switch (info.scriptHandler) {
        case "Violentmonkey":
          return [
            info.platform.browserName,
            info.platform.browserVersion,
            info.platform.os,
            info.platform.mobile,
          ];
        case "Tampermonkey":
          return [
            info.userAgentData.brands[0].brand,
            info.userAgentData.brands[0].version,
            info.userAgentData.platform,
            info.userAgentData.mobile,
          ];
        default:
          // Other userscript managers, I don't use others so I dunno how/if they give this info.
          return [];
      }
    })();
    const scriptInfo = `${info.scriptHandler} (Version: ${info.version})`;
    const browserInfo =
      browser && browserVersion
        ? `${browser} (Version: ${browserVersion}` + (mobile ? " (mobile)" : "") + ")"
        : "No browser information found";
    const osInfo = os || "No OS info found";
    return `Running on  ${scriptInfo}, Browser: ${browserInfo}, OS: ${osInfo}`;
  }

  /* =============================== download.js stuff goes here =============================== */

  /**
   * Keeps track of current download operation progress.
   */
  #downloadProgress = {
    downloader: undefined,
    enqueued: 0,
    success: 0,
    failure: 0,
    onSuccess: () => {
      this.#downloadProgress.enqueued = Math.max(0, this.#downloadProgress.enqueued - 1);
      this.#downloadProgress.success += 1;
      this.#updateDownloadUiText();
      if (this.#downloadProgress.enqueued === 0) {
        this.#updateDownloadUiColor();
      }
    },
    onFailure: error => {
      this.#downloadProgress.enqueued = Math.max(0, this.#downloadProgress.enqueued - 1);
      this.#downloadProgress.failure += 1;
      console.error(error);
      this.#updateDownloadUiText();
      if (this.#downloadProgress.enqueued === 0) {
        this.#updateDownloadUiColor();
      }
    },
  };
  /**
   * UI element that displays download progress.
   * @type {!HTMLSpanElement}
   */
  #downloadUi = this.#createDownloadUi();

  /**
   * Creates an instance of {@link TemplateParameters} with filled values
   * for {@link Congalala.DEFAULT_PLACEHOLDERS}
   * @param {!Location|HTMLAnchorElement} url the url to parse the metadata from
   * @param {(number|string|undefined)} timestamp Value to use for the placeholder
   *   {@link DEFAULT_PLACEHOLDERS.DownloadTimestamp}, if left empty uses {@link Date#now()}
   * @return {!TemplateParameters} the created TemplateParameters instance
   */
  static createDefaultTemplateParameters(url, timestamp) {
    const pathSegments = url.pathname.split("/");
    const fileName = pathSegments.pop();
    const extension = fileName.split(".").pop();
    // x => x filters out empty strings on the array that may occur from the string#split call
    const pathToFile = pathSegments.filter(x => x).join("/");
    const parameters = new TemplateParameters()
      .addParameter(Congalala.DEFAULT_PLACEHOLDERS.FileName, fileName)
      .addParameter(Congalala.DEFAULT_PLACEHOLDERS.Host, url.host)
      .addParameter(Congalala.DEFAULT_PLACEHOLDERS.Path, pathToFile)
      .addParameter(Congalala.DEFAULT_PLACEHOLDERS.DownloadTimestamp, timestamp || Date.now())
      .addParameter(Congalala.DEFAULT_PLACEHOLDERS.Extension, extension);
    return parameters;
  }

  /**
   * Turns a string URL into an anchor element so it can be passed into
   * #createDefaultTemplateParameters as url parameter.
   * @param {!string} src The URL to convert
   * @returns {!HTMLAnchorElement}
   */
  static urltoa(src) {
    const a = document.createElement("a");
    a.href = src;
    return a;
  }

  /**
   * Updates the text/progress of {@link #downloadProgress}.
   */
  #updateDownloadUiText() {
    const q = "Q: " + this.#downloadProgress.enqueued;
    const d = "D: " + this.#downloadProgress.success;
    const f = "F: " + this.#downloadProgress.failure;
    this.#downloadUi.innerText = `${q} | ${d} | ${f}`;
  }

  /**
   * Updates font color of {@link #downloadProgress} according to download progress.
   */
  #updateDownloadUiColor() {
    const color = (() => {
      if (this.#downloadProgress.enqueued > 0) {
        return Congalala.COLORS.Foreground;
      } else if (this.#downloadProgress.failure > 0) {
        return Congalala.COLORS.Failure;
      } else if (this.#downloadProgress.success > 0) {
        return Congalala.COLORS.Success;
      } else {
        return Congalala.COLORS.Foreground;
      }
    })();
    this.#downloadUi.style.color = color;
    this.#downloadUi.style.borderColor = color;
  }

  /**
   * Creates and styles a {@link HTMLSpanElement} to show current download progress.
   * Does not attach the element to the document.
   * @return {!HTMLSpanElement}
   */
  #createDownloadUi() {
    const downloadStateUi = document.createElement("span");
    downloadStateUi.id = "fellomen-Congalala-download-state";
    downloadStateUi.innerText = "Starting downloads...";
    downloadStateUi.style.position = "fixed";
    downloadStateUi.style.bottom = "4px";
    downloadStateUi.style.left = "4px";
    downloadStateUi.style.overflow = "hidden";
    downloadStateUi.style.padding = "4px";
    downloadStateUi.style.margin = "4px";
    downloadStateUi.style.border = "2px solid " + Congalala.COLORS.Foreground;
    downloadStateUi.style.outline = "4px solid " + Congalala.COLORS.Background;
    downloadStateUi.style.color = Congalala.COLORS.Foreground;
    downloadStateUi.style.background = Congalala.COLORS.Background;
    downloadStateUi.style.zIndex = 9999;
    downloadStateUi.style.fontFamily = "jetbrains mono, monospace";
    return downloadStateUi;
  }

  /**
   * Enqueues all passed files to be downloaded sequentially. Will not start/queue any further
   * downloads while there's is currently files being downloaded.
   * @param {DownloadInfo[]} files files to enqueue and download
   */
  #startQueuedDownload(files) {
    if (this.#downloadProgress.enqueued > 0) {
      console.warn("Download is already running, aborting repeat attempt.");
      return;
    }
    const debug = this.#config.get(Congalala.ConfigKeys.DebugMode);
    this.#downloadProgress.enqueued = debug ? 0 : files.length;
    this.#downloadProgress.success = 0;
    this.#downloadProgress.failure = 0;
    if (!debug && !this.#downloadUi.isConnected) {
      document.body.appendChild(this.#downloadUi);
    }
    this.#updateDownloadUiColor();

    this.#downloadProgress.downloader = new Downloader(
      this.#config.get(Congalala.ConfigKeys.FileNameTemplate) || this.#defaultTemplate,
    );
    this.#downloadProgress.downloader.enableQueue();
    files.forEach(file =>
      this.#downloadProgress.downloader.downloadWithTemplate(
        file.src,
        file.parameters,
        () => this.#downloadProgress.onSuccess(),
        e => this.#downloadProgress.onFailure(e),
        //referer,
      ),
    );
    if (debug) {
      const queuedDownloads = this.#downloadProgress.downloader.queue
        .map(x => x.url + " => " + x.name)
        .join("\n");
      console.debug("Enqueued downloads:\n" + queuedDownloads);
    } else {
      this.#downloadProgress.downloader.downloadQueue();
    }
  }

  /* ==================================== DOM and JS helpers ==================================== */

  /**
   * Creates a {@link MutationObserver} to iterate over all added nodes, with an attached callback
   * to iterate over all added nodes.
   * @param {function(Node, MutationRecord):void} callback Callback called for each added node
   * @param {?function(Node, MutationRecord):boolean} filter Optionally, a filter for which nodes
   *                                                         you want to receive
   * @returns {!MutationObserver}
   */
  static createAddedNodesObserver(callback, filter) {
    return Congalala.#iterateMutationNodes(callback, filter, mutation => mutation.addedNodes);
  }

  /**
   * Creates a {@link MutationObserver} to iterate over all removed nodes, with an attached callback
   * to iterate over all added nodes.
   * @param {function(Node, MutationRecord):void} callback Callback called for each added node
   * @param {?function(Node, MutationRecord):boolean} filter Optionally, a filter for which nodes
   *                                                         you want to receive
   * @returns {!MutationObserver}
   */
  static createdRemovedNodesObserver(callback, filter) {
    return Congalala.#iterateMutationNodes(callback, filter, mutation => mutation.removedNodes);
  }

  static #iterateMutationNodes(callback, filter, targetNodes) {
    return new MutationObserver(mutations =>
      mutations.forEach(mutation => {
        targetNodes(mutation).forEach(node => {
          if (filter?.(node, mutation)) {
            callback(node, mutation);
          }
        });
      }),
    );
  }

  /**
   * Convenience method to check if the value {@link HTMLElement.querySelector()}
   * is defined.
   * @param {!string} selector - CSS selector to run
   * @param {function(HTMLElement):void} fn - callback to call when the selector matches an element
   * @param {(HTMLElement|undefined)} target - the element on which to run .querySelector(), if undefined uses {@link document}.
   */
  static select(selector, fn, target) {
    const element = (target || document).querySelector(selector);
    if (element) {
      fn(element);
    }
  }
}

/**
 * File information to pass into {@link Congalala.collectFiles}.
 * @typedef {Object} FileInfo
 * @property {!string} src - URL for where to download the file from.
 * @property {!TemplateParameters} parameters - Parameter values to fill into the template when downloading.
 */
