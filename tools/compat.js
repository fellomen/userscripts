/**
 * A compatability wrapper around GM_* or GM.* methods to work around differences in userscript
 * engines or availability of the function
 */
class GM_Compat {
  static get VERSION() {
    return "2.0.0";
  }

  /**
   * Will attempt to add a style via GM_addStyle, and if that fails attempts
   * to manually add a style tag.
   * @param {!string} css - css rule to add
   */
  static addStyle(css) {
    if (typeof GM_addStyle == "function") {
      GM_addStyle(css);
    } else if (typeof GM == "object" && typeof GM.addStyle == "function") {
      GM.addStyle(css);
    } else {
      const id = "GM_compat-addstyle";
      let style = document.head.querySelector(`style#${id}`);
      if (style === null) {
        style = document.createElement("style");
        style.id = id;
        document.head.appendChild(style);
      }
      style.sheet.insertRule(css);
    }
  }

  /**
   * Will attempt to open a new tab in order with: GM_openInTab, GM.openInTab,
   * and lastly window.open.
   * @param {!string} url
   * @param {?Object} options
   */
  static openInTab(url, options) {
    if (typeof GM_openInTab == "function") {
      GM_openInTab(url, options);
    } else if (typeof GM == "object" && typeof GM.openInTab == "function") {
      GM.openInTab(url, options);
    } else {
      window.open(url, "_blank");
    }
  }

  /**
   * Puts a promise wrapper around GM_xmlhttpRequest and GM.xmlHttpRequest - whichever is available.
   * Check https://violentmonkey.github.io/api/gm/#gm_xmlhttprequest for details/return value.
   * If neither GM_xmlhttpRequest nor GM.xmlHttpRequest are available the promise will instantly
   * reject.
   * @param {!Object} options
   * @returns {!Promise}
   */
  static xmlHttpRequest(details) {
    return new Promise((resolve, reject) => {
      const rejectCallback = res => reject({ status: res.status, statusText: res.statusText });
      details.onabort = rejectCallback;
      details.onerror = rejectCallback;
      details.ontimeout = rejectCallback;
      details.onload = response => resolve(response);
      if (typeof GM_xmlhttpRequest == "function") {
        GM_xmlhttpRequest(details);
      } else if (typeof GM == "object" && typeof GM.xmlHttpRequest == "function") {
        GM.xmlHttpRequest(details);
      } else {
        reject("Neither GM_xmlhttpRequest nor GM.xmlHttpRequest are available");
      }
    });
  }

  /**
   * Registers a command in the Userscript add-ons popup menu.
   * @param {!string} name - the string to show in the menu entry.
   * @param {!function(!MouseEvent|!KeyEvent)} callback - callback when the entry is clicked on.
   * @throws, if neither GM_registerMenuCommand nor GM.registerMenuCommand are available.
   * Most lilkely because you are missing (at)grants in your userscript.
   */
  static registerMenuCommand(name, callback, accessKey) {
    if (typeof GM_registerMenuCommand == "function") {
      GM_registerMenuCommand(name, callback, accessKey);
    } else if (typeof GM == "object" && typeof GM.registerMenuCommand == "function") {
      GM.registerMenuCommand(name, callback, accessKey);
    } else {
      throw "No register menu command available.";
    }
  }
  /**
   * Returns the GM_info/GM.info object or undefined if neither of them are available.
   * @returns {!object} see {@link https://violentmonkey.github.io/api/gm/#gm_info}
   * @throws if neither GM_info nor GM.info are available. Most likely because you are
   * missing (at)grants in your userscript.
   */
  static info() {
    if (typeof GM_info == "object") {
      return GM_info;
    } else if (typeof GM == "object" && typeof GM.info == "object") {
      return GM.info;
    } else {
      throw "No GM info object available.";
    }
  }

  /**
   * Sets the clipboard value to the provided data.
   * @param {!string} data - the text/data to paste to the clipboard
   * @param {?string} info - mime type of the data. Not supprted on Greasemonkey
   * @todo Use a input/selection to copy to clipboard without grants.
   * @throws if neither GM_setClipboard nor GM.setClipboard are available,
   * this will be removed once the above todo is solved.
   */
  static setClipboard(data, info) {
    if (typeof GM_setClipboard == "function") {
      GM_setClipboard(data, info);
    } else if (typeof GM == "object" && typeof GM.setClipboard == "function") {
      // Greasemonkey does not support setting info and callback, manually call it instead
      GM.setClipboard(data, info);
    } else {
      throw "No set clipboard available.";
    }
  }

  /**
   * Downloads a file with the given details, which requires at least
   * an `url` (string) and `name` (string) parameter.
   * For full details see: {@link https://violentmonkey.github.io/api/gm/#gm_download}
   *
   * Note: At time of writing only Tampermonkey supports subfolders in the name parameter.
   *
   * Will fall back to download via adding (and removing afterwards) an anchor element if
   * neither GM_download nor GM.download are available.
   * @param {!object} details
   */
  static download(details) {
    if (typeof GM_download == "function") {
      GM_download(details);
    } else if (typeof GM == "object" && typeof GM.download == "function") {
      GM.download(details);
    } else {
      const a = document.createElement("a");
      a.href = details.url;
      a.download = details.name || details.url.split("/").pop();
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
  }
}
