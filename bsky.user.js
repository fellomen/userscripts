// ==UserScript==
// @name         Fell Omen's better bsky.app
// @namespace    fellomen
// @version      1.0.1
// @description  Improve bsky.app lurking
// @author       fellomen
// @match        https://bsky.app/*
// @match        https://cdn.bsky.app/*
// @icon         https://web-cdn.bsky.app/static/favicon.png
// @grant        GM_addStyle
// @grant        GM.addStyle
// @grant        GM_openInTab
// @grant        GM.openInTab
// @grant        GM_registerMenuCommand
// @grant        GM.registerMenuCommand
// @require      https://gitlab.com/fellomen/userscripts/-/raw/compat.js-1.4.0/tools/compat.js
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @updateURL    https://gitlab.com/fellomen/userscripts/-/raw/master/bsky.user.js
// @downloadURL  https://gitlab.com/fellomen/userscripts/-/raw/master/bsky.user.js
// @supportURL   https://gitlab.com/fellomen/userscripts
// @homepageURL  https://gitlab.com/fellomen/userscripts
// ==/UserScript==

(() => {
   'use strict';

   const CssClasses = {
       HandledImage: "fellomen_bsky_handled",
   }

   const ConfigKeys = {
        PreferredEvent: "Preferred_Event",
        DebugMode: "Debug_Mode",
        ApplyCustomFileName: "Apply_Custom_File_Name",
   }

    const config = new GM_config({
       "id": "fellomen-bsky_user_js-config",
       "title": "Fell Omen's better bsky.app",
       "fields": {
           [ConfigKeys.PreferredEvent]: {
               "label": "Preferred event",
               "title": "Which button should be used to run the custom click handling on images. Requires a page reload to take effect.",
               "type": "select",
               "options": ["Middle Mouse Button", "Left Mouse Button"],
               "default": "Middle Mouse Button",
           },
           [ConfigKeys.ApplyCustomFileName]: {
               "label": "Apply custom file name",
               "labelPos": "right",
               "title": "Enable to append a custom file name at the end of image files when opened via the userscript.",
               "type": "checkbox",
               "default": true,
           },
           [ConfigKeys.DebugMode]: {
               "label": "Debug Mode",
               "labelPos": "right",
               "title": "May display/log some additional information for debugging purposes.",
               "type": "checkbox",
               "default": false,
           },
       },
       "css": `
       * { padding: 4px; }
       .config_header {
           text-align: center;
           font-size: 24;
           padding: 8;
       }
       #fellomen-twitter_user_js-config_buttons_holder {
           padding: 40 20 10 20;
           width: 100%;
           text-align: center;
       }
       #fellomen-twitter_user_js-config_buttons_holder > * {
           display: inline-block;
           margin: 8 0;
       }
       body {
           background: #000000ee !important;
           border: 2px solid #2f3336;
           color: #e7e9ea !important;
       }
       .reset { color: #e7e9ea !important }
       `,
       "events": {
           "init": onConfigInitialized
       }
   });

   class PostMetaData {
       constructor(post, id, userDisplayName, userHandle) {
           this.post = post;
           this.id = id;
           this.userDisplayName = userDisplayName;
           this.userHandle = userHandle;
       }
   }

    function onConfigInitialized() {
        GM_Compat.registerMenuCommand(`Script settings`, (evt) => {
            config.open();
        }, "p");

        if (document.location.host === "cdn.bsky.app") {
            const backNavigation = window.performance.navigation.type === 2;
            if (!backNavigation) {
                const url = document.location.href;
                const newUrl = (() => {
                    if (url.includes("#")) { // take care of custom file name overrides
                        const parts = url.split("#");
                        return `${origify(parts[0])}#${parts[1]}`;
                    }
                    return origify(url);
                })();
                if (url !== newUrl) {
                    document.location.href = newUrl;
                }
            }
            addBackLinksToRawFiles();
        } else {
            injectIntoPosts();
            if (config.get(ConfigKeys.DebugMode)) {
                    GM_Compat.addStyle(`.${CssClasses.HandledImage} { border: 4px dotted orange }`);
            }
        }
    }

    function addBackLinksToRawFiles() {
        // only works when custom file name is supplied
        if (document.location.href.includes("#")) {
            const customFileName = document.location.href.split("#").pop();
            const parts = decodeURI(document.location.href.split("#").pop()).split(" ");
            const userHandle = parts[0].replace("@", "");
            const postId = parts[1].replace(/\(|\)|(_p\d+)/g, "");

            GM_Compat.registerMenuCommand(`Go back to user: ${userHandle}`, (evt) => {
                document.location.href = `https://bsky.app/profile/${userHandle}`;
            }, "u");
            GM_Compat.registerMenuCommand(`Go back to post: ${postId}`, (evt) => {
                document.location.href = `https://bsky.app/profile/${userHandle}/post/${postId}`;
            }, "p");

            document.addEventListener("keydown", (evt) => {
                if (evt.code === "KeyU" && evt.ctrlKey) {
                    evt.preventDefault();
                    document.location.href = `https://bsky.app/profile/${userHandle}`;
                } else if (evt.code === "KeyP" && evt.ctrlKey) {
                    evt.preventDefault();
                    document.location.href = `https://bsky.app/profile/${userHandle}/post/${postId}`;
                }
            });
        }
    }

    function injectIntoPosts() {
        new MutationObserver((mutations, observer) => {
            iterateAddedNodes(mutations, (node, mutation) => {
                if (node.tagName === "DIV") {
                    node.querySelectorAll("img").forEach(img => addOpenImageOverride(img));
                }
            });
        }).observe(document.body, { attributes: false, childList: true, subtree: true });
    }

    function iterateAddedNodes(mutations, callback, filter) {
        mutations.forEach(mutation => {
            mutation.addedNodes.forEach(node => callback(node, mutation));
        });
    }

    /**
     * Adds an event lister to override the default behaviour.
     * @param {HTMLImageElement} img Target element to add the event lister to
     */
    function addOpenImageOverride(img) {
        img.addEventListener("click", (evt) => {
            // Seems bsky.app is doing some fuckery here and sends auxclick to the click event
            // Difference of the event between middle and left click I found are (for middle click):
            // metaKey = true
            // clientX, clientY, layerX and layerY are all 0
            const isMiddleMouseButton = event.metaKey;
            const prefersMiddleMouseButton = config.get(ConfigKeys.PreferredEvent) == "Middle Mouse Button";
            if (isMiddleMouseButton === prefersMiddleMouseButton) {
                evt.preventDefault();
                evt.stopImmediatePropagation();
                const targetUrl = origify(img.src) + (() => {
                    if (config.get(ConfigKeys.ApplyCustomFileName)) {
                        const override = buildOverrideFileName(img);
                        return override === undefined ? "" : "#" + override;
                    }
                    return "";
                })();
                debugLog("Opening: " + targetUrl);
                GM_Compat.openInTab(targetUrl, true);
            }
        });
        img.classList.add(CssClasses.HandledImage);
    }

    function origify(imageUrl) {
        return imageUrl.replace("feed_thumbnail", "feed_fullsize");
    }

    function walkUpDomUntil(element, filter) {
        if (element === null || element === undefined) {
            return undefined;
        } else if (filter(element)) {
            return element;
        } else {
            return walkUpDomUntil(element.parentElement, filter);
        }
    }

    /**
     * @param {HTMLImageElement} img element from which to start searching for a post element
     */
    function getMetadata(img) {
        const post = walkUpDomUntil(img, element => {
            // Posts (currently) only have 1 link to themselves.
            // But images, which are not part of a post would reach an element
            // further up the DOM hierarchy which would find links to multiple posts.
            return element.tagName === "DIV" && element.querySelectorAll("a[href*='/post/']").length === 1;
        });
        if (post === undefined) {
            if (document.location.href.includes("/post/")) {
                // Special case for the post view:
                // The primary post of that page will not have a backlink
                // to itself. However all other parents/replies do.
                const postViewPrimary = walkUpDomUntil(img, element => {
                    // Find the parent div that also contains user display name + user handle divs
                    // Unfortunately we a also have to dodge the bottom "x likes", "x reposts", etc. divs.
                    // Unlike the user name ones, these do contain a further span element with the count.
                    return element.tagName === "DIV" && [...element.querySelectorAll("div[role='link'] > div")].some(x => x.childElementCount === 0);
                });
                const roleLink = postViewPrimary.querySelectorAll("div[role='link']");
                return new PostMetaData(
                    postViewPrimary,
                    document.location.href.split("/").pop(),
                    roleLink[0].innerText.trim(),
                    roleLink[1].innerText.trim(),
                )
            }
            return;
        }
        const anchors = post.querySelectorAll("a[href^='/profile']");
        return new PostMetaData(
            post,
            anchors[2].href.split("/").pop(),
            anchors[0].innerText.trim(),
            anchors[1].innerText.trim(),
        )
    }

    function buildOverrideFileName(img) {
        if (img.src.includes("/banner/")) {
            // Special case for banner imagers - for now just assume these are only on profiles
            return `@${document.location.href.split("/").pop()} (banner) ${sanitizeFileName(img.src)}`;
        }
        if (img.src.includes("/avatar/")) {
            // Special case for profile pics - for now just assume these are only on profiles
            return `@${document.location.href.split("/").pop()} (pfp) ${sanitizeFileName(img.src)}`;
            return;
        }
        const metadata = getMetadata(img);
        if (metadata === undefined) {
            console.error("Could not parse post metadata - skipping custom file name.");
            return;
        }
        const index = [...metadata.post.querySelectorAll("img")].indexOf(img);
        return `${metadata.userHandle} (${metadata.id}_p${index}) ${sanitizeFileName(img.src)}`;
    }

    function sanitizeFileName(fullFileName) {
        const split = fullFileName.split("/").pop().split("@");
        const ext = split.pop();
        const fileName = split.pop();
        return fileName + "." + ext;
    }

    function debugLog(message) {
        if (config.get(ConfigKeys.DebugMode)) {
            console.log(message);
        }
    }
})();