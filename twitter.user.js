// ==UserScript==
// @name         twitter.com
// @namespace    fellomen
// @version      1.4.1
// @description  Improved twitter lurking
// @author       fellomen
// @match        https://x.com/*
// @match        https://twitter.com/*
// @match        https://pbs.twimg.com/media/*
// @match        https://video.twimg.com/ext_tw_video/*
// @match        https://video.twimg.com/amplify_video/*
// @grant        GM_openInTab
// @grant        GM.openInTab
// @grant        GM_xmlhttpRequest
// @grant        GM.xmlHttpRequest
// @grant        GM_registerMenuCommand
// @grant        GM.registerMenuCommand
// @grant        GM_addStyle
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM.getValue
// @grant        GM.setValue
// @grant        GM_info
// @grant        GM.info
// @grant        GM_setClipboard
// @grant        GM.setClipboard
// @require      https://gitlab.com/fellomen/userscripts/-/raw/compat.js-1.4.0/tools/compat.js
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @updateURL    https://gitlab.com/fellomen/userscripts/-/raw/master/twitter.user.js
// @downloadURL  https://gitlab.com/fellomen/userscripts/-/raw/master/twitter.user.js
// @supportURL   https://gitlab.com/fellomen/userscripts
// @homepageURL  https://gitlab.com/fellomen/userscripts
// ==/UserScript==

(() => {
    const RegEx = {
        Twitter: /^https:\/\/(twitter|x)\.com\/.*$/,
        Image: /^https:\/\/pbs\.twimg\.com\/media\/(?<imageId>[a-zA-Z0-9-_]+)($|\?[a-z0-9\?&=]*|\.[a-z]+(:\w+)?)/,
        Video: /^https:\/\/video\.twimg\.com\/(ext_tw|amplify)_video\/(?<videoId>[a-zA-Z0-9-_]+)\/(pu\/)?vid\/(?<resolution>\d+x\d+)\/(?<fileName>[a-zA-Z0-9-_]+)(?<extension>\.mp4)(\?tag=\d+)?/,
        Gallery: /^https:\/\/(twitter|x)\.com\/(?<userHandle>.*?)\/status\/(?<postId>\d+)\/photo\/(?<page>\d+)$/
    }

    const ConfigKeys = {
        PreferredEvent: "Preferred_Event",
        ApplyCustomFileName: "Apply_Custom_File_Name",
        DebugMode: "Debug_Mode",
        Generate_ytdlp_Command: "Generate_ytdlp_Command",
        Ytdlp_Command_Template: "Ytdlp_Command_Template",
        Enable_Custom_Navigation: "Enable_Custom_Navigation",
        Open_External_Links: "Open_External_Links"
    }

    const CssClass = {
        PostPluginButton: "fellomen-post-plugin"
    }

    const config = new GM_config({
        "id": "fellomen-twitter_user_js-config",
        "title": createConfigHeader(),
        "fields": {
            "Preferred_Event": {
                "label": "Preferred event",
                "title": "Which button should be used to run the custom click handling on images. Note: LMB will overwrite Twitter's default behaviour",
                "type": "select",
                "options": ["Middle Mouse Button", "Left Mouse Button"],
                "default": "Middle Mouse Button",
            },
            "Generate_ytdlp_Command": {
                "label": "Generate a <a target=\"_blank\" href=\"https://github.com/yt-dlp/yt-dlp\">yt-dlp</a> command to download videos.",
                "labelPos": "right",
                "title": "Instead of trying to redirect to the video file, generates a command to download the video via yt-dlp. Needs to be run locally, but will be more resistant to changes by Twitter.",
                "type": "checkbox",
                "default": "false",
            },
            "Ytdlp_Command_Template": {
                "label": "yt-dlp command template",
                "labelPos": "left",
                "title":"Available placeholders: {browser}, {fileName}, {url}",
                "type":"text",
                "default":"yt-dlp --embed-metadata --embed-subs --embed-thumbnail --cookies-from-browser {browser} -o \"{fileName}\" {url}"
            },
            "Apply_Custom_File_Name": {
                "label": "Apply custom file name",
                "labelPos": "right",
                "title": "Enable to append a custom file name at the end of image/video files when opened via the userscript.",
                "type": "checkbox",
                "default": true,
            },
            "Enable_Custom_Navigation": {
                "label": "Use custom arrow key/wasd navigation",
                "labelPos": "right",
                "title": "Up/Down/W/S to navigate in the feed. Left/A to like a post. Right/D to press the plogon button.",
                "type": "checkbox",
                "default": false,
            },
            "Open_External_Links": {
                "label": "Also opens external links when using the plogon button.",
                "labelPos": "right",
                "type": "checkbox",
                "default": false,
            },
             "Debug_Mode": {
                 "label": "Debug mode",
                 "labelPos": "right",
                 "title": "Will usually only add some logging to the console.",
                 "type": "checkbox",
                 "default": false,
             }
        },
        "css": `
        * { padding: 4px; }

        .config_header {
            text-align: center;
            font-size: 24;
            padding: 8;
        }
        #fellomen-twitter_user_js-config_buttons_holder {
            padding: 40 20 10 20;
            width: 100%;
            text-align: center;
        }
        #fellomen-twitter_user_js-config_buttons_holder > * {
            display: inline-block;
            margin: 8 0;
        }

        body {
            background: #000000ee !important;
            border: 2px solid #2f3336;
            color: #e7e9ea !important;
        }
        .reset { color: #e7e9ea !important }
        `,
        "events": {
            "init": onConfigInitialized
        }
    });

    function createConfigHeader() {
        const container = document.createElement("div");
        const header = document.createElement("h3");
        header.innerText = "twitter.user.js settings"
        header.style.margin = "10px";
        const wikiLink = document.createElement("a");
        wikiLink.innerText = "Link to wiki"
        wikiLink.href = "https://gitlab.com/fellomen/userscripts/-/wikis/twitter.user.js";
        wikiLink.target = "_blank";
        wikiLink.ref = "noopener noreferrer"
        wikiLink.style.fontSize = "16pt";
        const apology = document.createElement("span");
        apology.innerText = "peak design";
        apology.style.fontSize = "9pt";
        apology.title = "I'm sorry this looks kinda bad, I'll get around to fix this at some point."

        container.appendChild(header);
        container.appendChild(wikiLink);
        container.appendChild(document.createElement("br"));
        container.appendChild(apology);
        return container;
    }

    function onConfigInitialized() {

        // Twitter just rewrites the current page via AJAX when navigating on the page
        // The approach of loading specific script parts on page load does not work with that.
        // Listen for URL changes and en/disable features accordingly.
        // Changed the script to initially run on all twitter.com pages. Currently there is
        // nothing that needs to run on a particular page only.
        // The below code snippet can be used to track location changes in case it's
        // ever needed to actually react to specific pages.

        // window.addEventListener("load", () => {
        //     let href = document.location.href;
        //     new MutationObserver((mutations, observer) => {
        //         if (document.location.href !== href) {
        //             href = document.location.href;
        //              // TODO: code here
        //         }
        //     }).observe(document.body, { attributes: false, childList: true, subtree: true });
        // });

        GM_Compat.registerMenuCommand(`Script settings`, (evt) => {
            config.open();
        }, "p");

        if (RegEx.Twitter.test(document.location.href)) {
            debuglog(`Running script on twitter page: ${document.location.href}`);
            injectIntoPosts();
            cleanUpRightSideBar();

            if (config.get(ConfigKeys.Enable_Custom_Navigation)) {
                addCustomNavigation();
            }

            // On post pages the "main" post has some padding on their buttons
            // -> Add this for the userscript button
            GM_Compat.addStyle(`a.${CssClass.PostPluginButton} {
            display: flex;
            align-items: center;
            justify-content: center;
        `);
        } else if (RegEx.Image.test(document.location.href)) {
            debuglog(`Running script on image page: ${document.location.href}`);
            const backNavigation = window.performance.navigation.type === 2;
            if (!backNavigation) { // do not act on back navigations.
                const url = document.location.href;
                let newUrl = document.location.href;
                if (url.includes("#")) { // take care of personal file name overrides
                    const parts = url.split("#");
                    newUrl = `${origify(parts[0])}#${parts[1]}`;
                } else {
                    newUrl = origify(url);
                }
                if (url !== newUrl) {
                    document.location.href = newUrl;
                }
            }
            addBackLinksToRawFiles();
        } else if (RegEx.Video.test(document.location.href)) {
            debuglog(`Running script on video page: ${document.location.href}`);
            addBackLinksToRawFiles();
        } else {
            debuglog(`Not executing any scripts for: ${document.location.href}`);
        }
    }


    function addBackLinksToRawFiles() {
        // only works when custom file name is supplied
        if (document.location.href.includes("#")) {
            const customFileName = document.location.href.split("#").pop();
            const parts = decodeURI(document.location.href.split("#").pop()).split(" ");
            const userHandle = parts[0];
            const postId = parts[1].replace(/\(|\)|(_p\d+)/g, "");

            GM_Compat.registerMenuCommand(`Go back to user: ${userHandle}`, (evt) => {
                document.location.href = `https://twitter.com/${userHandle}`;
            }, "u");
            GM_Compat.registerMenuCommand(`Go back to post: ${postId}`, (evt) => {
                document.location.href = `https://twitter.com/${userHandle}/status/${postId}`;
            }, "p");

            document.addEventListener("keydown", (evt) => {
                if (evt.code === "KeyP" && evt.ctrlKey) {
                    evt.preventDefault();
                    document.location.href = `https://twitter.com/${userHandle}/status/${postId}`;
                } else if (evt.code === "KeyU" && evt.ctrlKey) {
                    evt.preventDefault();
                    document.location.href = `https://twitter.com/${userHandle}`;
                }
            });
        }
    }

    function cleanUpRightSideBar() {
        // Remove the "Get Verified" section on the right side bar
        hide('[aria-label="Get Verified"]');
        // Remove the "Trends for you" section on the right side bar
        hide('[aria-label="Timeline: Trending now"]');
    }

    function addCustomNavigation() {
        function getStartingPost() {
            // have to start with an article because everything else is just divs
            const article = document.querySelector("article");
            if (article === null) {
                errorlog("Custom navigation: Did not find an article to latch onto.");
                return null;
            }
            // kinda ugly, but it is what it is
            return article.parentNode.parentNode.parentNode;
        }

        function select(post) {
            if (post) {
                post.style.background = "#00f91026";
                post.scrollIntoView({ block: "center" });
            }
            return post;
        }

        function moveTo(current, selector, description) {
            var target = current;
            do {
                target = selector(target);
                if (target === null) {
                    errorlog(`Could not move to ${description} post.`);
                    return;
                }
                // keep moving to next until we hit an actual post - not ads or other shit
            } while(target.querySelector("article") === null);
            current.style.background = null;
            return select(target);
        }

        // this probably needs a better name
        function before(evt, selected, execute) {
            evt.preventDefault();
            if (selected == null) {
                return select(getStartingPost());
            } else {


                const result = execute(selected);
                if(!result) {
                    if (!document.body.contains(selected)) {
                        // if the user scrolled without using the navigation
                        // or navigated to another page we keep this reference,
                        // but it is no longer part of the actual DOM.
                        // In that case we should reset our selection to prevent
                        // a deadlock, where movements are completely inaccessible.
                        return select(getStartingPost());
                    } else {
                        // Hit the end of the feed
                        return selected;
                    }
                    //if (lastDocumentLocation !== document.location.href) {
                    //    lastDocumentLocation = document.location.href;
                    //    return select(getStartingPost());
                    //} else {
                    //    return selected;
                    //}
                }
                return result;
            }
        }

        var selectedPost = select(getStartingPost());
        document.addEventListener("keydown", evt => {
            console.log(evt);
            if (evt.target.tagName === "INPUT") {
                return; // don't use these events if the user is typing into any text field
            }
            if (evt.code === "ArrowDown" || evt.code === "KeyS") {
                selectedPost = before(evt, selectedPost, (post) => moveTo(post, x => x.nextSibling, "next"));
            } else if (evt.code === "ArrowUp" || evt.code === "KeyW") {
                selectedPost = before(evt, selectedPost, (post) => moveTo(post, x => x.previousSibling, "previous"));
            } else if (evt.code === "ArrowLeft" || evt.code === "KeyA") {
                selectedPost = before(evt, selectedPost, (post) => {
                    [...post.querySelectorAll("[role='button']")].forEach(button => {
                        console.log(button);
                        if (button.ariaLabel != null && button.ariaLabel.includes("Like")) {
                            button.click();
                        }
                    });
                    return post;
                });
            } else if (evt.code === "ArrowRight" || evt.code === "KeyD") {
                selectedPost = before(evt, selectedPost, (post) => {
                    post.querySelector(`.${CssClass.PostPluginButton}`).click();
                    return post;
                });
            } else if (evt.code === "Space") {
                if (selectedPost) {
                    const playButton = selectedPost.querySelector("div[aria-label^='Play ']");
                    if (playButton) {
                        evt.preventDefault();
                        playButton.click();
                    }
                }
            }
        });
    }

    function injectIntoPosts() {
        new MutationObserver((mutations, observer) => {
            iterateAddedNodes(mutations, (node, mutation) => {
                const article = node.querySelector("article");
                if (article) {
                    handlePost(article);
                    return;
                }
            });
        }).observe(document.body, { attributes: false, childList: true, subtree: true });
        // or images on specific elements in a way that isn't annoying to track
        new MutationObserver((mutations, observer) => {
            iterateAddedNodes(mutations, (node, mutation) => {
                if (node.tagName === "IMG") {
                    const trendingParent = walkUpDomUntil(node, (element) => {
                        const ariaLabel = element.attributes["aria-label"];
                        return ariaLabel && ariaLabel.value === "Trending";
                    });
                    if (trendingParent !== undefined) {
                        // Seems to be the only way to identify the gallery preview images (right side bar)
                        return;
                    }
                    const link = walkUpDomUntil(node, (element) => element.tagName === "A" && element.href.includes("/photo/"));
                    if (link !== undefined) {
                        // images in posts
                        link.addEventListener(getPreferredEvent(), (evt) => {
                            // After changes to the media tab don't prevent default behaviour.
                            if (!isInMediaTab()) {
                                evt.preventDefault();
                                openInNewTab(origify(node.src), node);
                            }
                        });
                    } else {
                        // All sorts of other images
                        if (node.src.includes("ext_tw_video_thumb") || node.src.includes("/emoji/") || !node.src.includes("twimg.com")) {
                            // do not put a listener on:
                            // - video thumbnails
                            // - emoji category icons in the emoji picker
                            // - any external images (ie giphy images)
                            return;
                        }
                        // Still affects: gallery images, user banner, user profile pic (last two do no actually work on the openInNewTab)
                        node.addEventListener("click", (evt) => {
                            const gallery = document.location.href.match(RegEx.Gallery);
                            if(gallery !== null){
                                openFromGalleryInNewTab(origify(node.src), gallery);
                            } else {
                                openInNewTab(origify(node.src), node);
                            }
                        });
                    }
                }
            });
        }).observe(document.body, { attributes: false, childList: true, subtree: true });
    }

    function openInNewTab(url, element) {
        let tweet = walkUpDomUntil(element, e => e.attributes["aria-labelledby"]);
        if (getQuoteUserHandle(tweet).length == 0) {
            // once again more issues, just try and see if it has the user info inside the div,
            // if yes its a quote tweet. If no, it isn't and we need to keep going up.
            tweet = walkUpDomUntil(tweet.parentElement, e => e.attributes["aria-labelledby"]);
        }

        const isQuoteTweet = tweet.tagName !== "ARTICLE";

        if (tweet === undefined) {
            debuglog(`Found no article/quote tweet div for URL to open: ${url} -> Skipping`);
        }
        if (!config.get(ConfigKeys.ApplyCustomFileName) || tweet === undefined) {
            GM_Compat.openInTab(url, true);
            return;
        }
        const postInfo = scrapePostInformation(tweet, isQuoteTweet);
        //todo: should try this with combined image + video posts as well
        const images = selectPostImagesFrom(tweet);
        const page = images.indexOf(element);
        const originalFileName = url.split("/").pop().split(/[:\?]/).shift();
        const overrideUrl = `${postInfo.userHandle} (${postInfo.postId}_p${page}) ${originalFileName}`;
        GM_Compat.openInTab(`${url}#${overrideUrl}`, true);
    }
    function openFromGalleryInNewTab(url, galleryMatch) {
        const originalFileName = url.split("/").pop().split(/[:\?]/).shift();
        const overrideUrl = `${galleryMatch.groups.userHandle} (${galleryMatch.groups.postId}_p${galleryMatch.groups.page}) ${originalFileName}`;
        GM_Compat.openInTab(`${url}#${overrideUrl}`, true);
    }

    async function getVideosOfPost(target, postInfo, isQuoteTweet, alreadyFetchedVideos) {
        // if auto play is disabled, twitter will not actually add a video tag until it starts playing
        // -> also check for the presence of "play" buttons.
        const videos = [];
        const videoElements = target.querySelectorAll("video");
        const playButtons = target.querySelectorAll("[aria-label^='Play Video']");
        const videoCount = videoElements.length + playButtons.length;

        if (videoCount > 0 && config.get(ConfigKeys.Generate_ytdlp_Command)) {
            // This might need some more testing for reliability across browsers/OS
            const browser = GM_Compat.info().userAgentData.brands[0].brand;

            const generatedCommand = config.get(ConfigKeys.Ytdlp_Command_Template)
                .replaceAll("{browser}", browser)
                .replaceAll("{url}", postInfo.postUrl)
                .replaceAll("{fileName}", `${postInfo.userHandle} (${postInfo.postId}_p%(playlist_index)s).%(ext)s`);
            GM_Compat.setClipboard(generatedCommand, "text");
        }
        // return an empty array so caller doesn't have to worry about this.
        return videos;
    }

    /**
     * @param target {Element} can either be an article if it's a normal tweet; or a div, if it's
     *    a quoted tweet.
     */
    function getImagesOfPost(target, postInfo, isQuoteTweet, alreadyFetchedImages) {
        const attachedImages = selectPostImagesFrom(target);
        const images = attachedImages
        .filter(img => !alreadyFetchedImages.some(x => x.url == img.src))
        .map(img => {
            const origified = origify(img.src);
            const page = attachedImages.indexOf(img);
            const originalFileName = origified.split("/").pop().split(/[:\?]/).shift();
            const overrideUrl = `${postInfo.userHandle} (${postInfo.postId}_p${page}) ${originalFileName}`;
            return { node: img, url: img.src, origified: origified, overrideUrl: overrideUrl };
        });
        return images;
    }

    function scrapePostInformation(article, isQuoteTweet) {
        // this is probably pretty scuffed since there's no good selectors and I do not trust
        // data-testid attributes to stay for a long time
        // also prefer to not rely on the URL (ie different kind of pages, retweets, etc.)
        const userHandle = scrapeUserHandle(article, isQuoteTweet);
        const timeElement = article.querySelector("time");
        const postUrl = scrapePostUrl(article, timeElement);
        const postId = postUrl.split("/").pop();
        // format: yyyyMMdd'T'HHmmss
        const dateTime = timeElement.dateTime.replaceAll(/([: -]|\.\d{3}Z)/g, "");
        return { userHandle: userHandle, postId: postId, dateTime: dateTime, postUrl: postUrl };
    }

    function scrapeUserHandle(article, isQuoteTweet) {
        if (isQuoteTweet) {
            // I didn't want to rely on data-testid fields, but Twitter really makes needlessly difficulty to select anything
            // in a nice way so fuck that shit.
            // Find all spans in the user name section - take the last two and take the handle (the other one is just padding)
            return [...getQuoteUserHandle(article)].splice(-2).shift().innerText;
        }
        return article.querySelector("a > div > span").innerText;
    }

    function getQuoteUserHandle(target) {
        return target.querySelectorAll("[data-testid='User-Name'] span");
    }

    function scrapePostUrl(article, timeElement) {
        if (timeElement.parentElement.href === undefined) {
            // this means we are looking at a quote retweet - this <time> is not inside an <a> that has a link back to the post itself
            // find the link on the image itself
            // todo: testing if this works with text and video as well
            // todo: test if this fixes the behaviour on the timeline
            const onQuoteLink = [...article.querySelectorAll("a")].filter(x => x.href.includes("/status/"))[0];
            if (onQuoteLink === undefined) {
                debuglog("Could not find anchor linking back to quoted tweet.");
                return undefined;
            }
            // the link would look like this: "https://twitter.com/Mumushui6/status/1678586255083466752/photo/1"
            //todo: I'm not sure if this works if there's a video in the quoted tweet,
            return onQuoteLink.href.replace(/\/(photo|video)\/\d$/, "");
        }
        return timeElement.parentElement.href;
    }

    function handlePost(article) {
        // bottom bar to attach custom button to
        const bottomBar = article.querySelector("[role='group']");
        if (bottomBar === null) {
            debuglog("article is not actually a post:")
            debuglog(article);
            return;
        }
        if (bottomBar.querySelector(`a.${CssClass.PostPluginButton}`)) {
            return;
        }

        // 2024-02-18: Disabled this behavior because changes to the link embeds make
        // this not work as expected anymore.
        // The only way to actually get the URL t.co links go to would be to send
        // a request to it and check wwhere it redirects to in the headers (HTTP 301 response)
        // fix all external links to not be masked with tracking site:
        //article.querySelectorAll("a").forEach(link => {
        //    if (link.href.startsWith("https://t.co/")) {
        //        // Twitter started abbreviating long links with a '…' character at the end
        //        link.href = link.innerText.replace("…", "");
        //    }
        //});

        const a = document.createElement("a");
        a.classList.add(CssClass.PostPluginButton);
        a.innerText = "🧩 Plogon";
        a.style.cursor = "pointer";
        a.title = "Open images and links in new tab. Video not yet supported.";
        a.style.color = "rgb(113, 118, 123)";
        a.addEventListener("click", async(evt) => {
            const quoteTweet = article.querySelector("div[aria-labelledby]");
            let links = selectLinksFromPost(article);
            let images = [];
            let videos = [];
            // Normal tweets also have a div[aria-labelledby], but their direct child has an id, unlike normal tweets.
            if (quoteTweet !== null && getQuoteUserHandle(quoteTweet).length > 0){
                const quoteInfo = scrapePostInformation(quoteTweet, true);
                images = images.concat(getImagesOfPost(quoteTweet, quoteInfo, true, images));
                videos = videos.concat(await getVideosOfPost(quoteTweet, quoteInfo, true, videos));
            }
            const tweetInfo = scrapePostInformation(article, false);
            images = images.concat(getImagesOfPost(article, tweetInfo, false, images));
            videos = videos.concat(await getVideosOfPost(article, tweetInfo, false, videos));
            const useOverrideUrl = config.get(ConfigKeys.ApplyCustomFileName);
            [].concat(links, images, videos).forEach(x => {
                GM_Compat.openInTab(useOverrideUrl ? `${x.origified}#${x.overrideUrl}` : x.origified, true);
            });
        });
        bottomBar.appendChild(a);
    }

    function selectLinksFromPost(article) {
        if (!config.get(ConfigKeys.Open_External_Links)) {
            return [];
        }

        // Twitter uses the t.co domain to mask external links
        // Internal links all go to twitter.com/ (now x.com/) so
        // we can use that to filter for external links.
        return [...article.querySelectorAll("a")]
            .filter(a => a.href.includes("t.co/"))
            .map(a => {
            return { origified: a.href , overrideUrl: "" }
        });

    }

    function selectPostImagesFrom(article) {
        // select "a img" -> filters out emojis (these do not resides inside a link)
        // -> issue: emojis in the display name (and in retweeter display name) are actually in a link
        // -> select "a div > img" -> emojis reside inside a span with the username.
        // -> only the profile pic + post images remain
        // -> splice the first image to get ride of the profile pic
        // return [...article.querySelectorAll("a div > img")].splice(1);
        // Problem: this still selects "organization" images which are part of the actual username link
        // -> just fetch all images on the article and filter by their src, which must contain /media/
        // for it to be an image attached to the post
        return [...article.querySelectorAll("img")].filter(i => i.src.includes("/media/"));
    }

    function hide(cssSelector, important) {
        GM_Compat.addStyle(`${cssSelector} { display: none ${important ? "!important" : ""}; }`)
    }

    function iterateAddedNodes(mutations, callback, filter) {
        mutations.forEach(mutation => {
            mutation.addedNodes.forEach(node => callback(node, mutation));
        });
    }

    function origify(url) {
        if (url.includes("/card_img/") || url.includes("profile_banners") || url.includes("profile_images")) {
            // card_img, profile_banners and profile_images images can't have the :orig applied
            return url;
        }
        if (/:\w+$/.test(url)) return url.replace(/:\w+$/, ":orig");
        if (url.includes("?")) { // new/mobile format
            const match = url.match(/(.*)\?.*format=(\w+).*/i);
            if (match != null) {
                return `${match[1]}.${match[2]}:orig`;
            }
        }
        return url + ":orig";
    }

    function walkUpDomUntil(element, filter) {
        if (element === null || element === undefined) {
            return undefined;
        } else if (filter(element)) {
            return element;
        } else {
            return walkUpDomUntil(element.parentElement, filter);
        }
    }

    function getPreferredEvent() {
        if (config.get(ConfigKeys.PreferredEvent) == "Middle Mouse Button") {
            return "auxclick"
        }
        return "click";
    }

    function isInMediaTab() {
        //const tabsHighlight = document.querySelectorAll("a[role=tab] span + div");
        //const activeTab = [...tabsHighlight].find(x => x.style.backgroundColor !== "");
        //return activeTab.previousElementSibling.innerText === "Media";
        return document.location.href.endsWith("/media");
    }

    function debuglog(message) {
        if (config.get(ConfigKeys.DebugMode)) {
            console.log(message);
        }
    }

    function errorlog(message) {
        console.log(message);
    }
})();