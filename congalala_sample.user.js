// ==UserScript==
// @name        Congalala Sample
// @namespace   fellomen
// @version     1.0.1
// @description Sample script for Congalala usage
// @author      fellomen
// @match       https://gitlab.com/fellomen/userscripts
// @noframes
// @grant        GM_download
// @grant        GM.download
// @grant        GM_registerMenuCommand
// @grant        GM.registerMenuCommand
// @grant        GM_info
// @grant        GM.info
// @require      https://gitlab.com/fellomen/userscripts/-/raw/compat.js-1.5.0/tools/compat.js
// @require      https://gitlab.com/fellomen/userscripts/-/raw/download.js-1.3.0/tools/download.js
// @require      https://gitlab.com/fellomen/userscripts/-/raw/congalala.js-1.0.0/tools/congalala.js
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @supportURL   https://gitlab.com/fellomen/userscripts
// @homepageURL  https://gitlab.com/fellomen/userscripts
// ==/UserScript==

(() => {
    'use strict';
    const collectFiles = () => {
        const files = document.querySelectorAll(".tree-content-holder a[href*='/-/blob/']");
        const dlTimestamp = Date.now();
        const downloadItems = [...files].map(anchor => ({
            src: anchor.href.replace("/-/blob/", "/-/raw/"),
            parameters: Congalala.createDefaultTemplateParameters(anchor, dlTimestamp),
        }));
        return downloadItems;
    };

    const congalala = new Congalala(collectFiles).init(() => {
        console.log("Script initialization finished");
    });
})();