// ==UserScript==
// @name         discord.com
// @namespace    fellomen
// @match        https://discord.com/channels/*
// @resource     USER_CSS https://gitlab.com/fellomen/userscripts/-/raw/main/discord.user.css
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @version      1.0
// @author       fellomen
// @description  Use some CSS trickery to hide nitro and other unnecessary UI elements.
// @updateURL    https://gitlab.com/fellomen/userscripts/-/raw/master/discord.user.js
// @downloadURL  https://gitlab.com/fellomen/userscripts/-/raw/master/discord.user.js
// @supportURL   https://gitlab.com/fellomen/userscripts
// ==/UserScript==
(() => {
    'use strict';

    // hide nitro/premium/bloat UI
    const userCss = GM_getResourceText("USER_CSS");
    GM_addStyle(userCss);

})();