// ==UserScript==
// @name         Conga: Steam Point Shop
// @namespace    fellomen
// @version      1.0.0
// @description  Download assets from steam point shop
// @author       fellomen
// @match        https://store.steampowered.com/points/shop/app/*
// @match        https://steamcommunity.com/id/*/?previewprofile=1&appid=*
// @grant        GM_download
// @grant        GM.download
// @grant        GM_registerMenuCommand
// @grant        GM.registerMenuCommand
// @grant        GM_info
// @grant        GM.info
// @grant        GM_getValue
// @grant        GM.getValue
// @grant        GM_setValue
// @grant        GM.setValue
// @require      https://gitlab.com/fellomen/userscripts/-/raw/compat.js-2.0.0/tools/compat.js
// @require      https://gitlab.com/fellomen/userscripts/-/raw/download.js-2.0.0/tools/download.js
// @require      https://gitlab.com/fellomen/userscripts/-/raw/congalala.js-1.1.0/tools/congalala.js
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @supportURL   https://gitlab.com/fellomen/userscripts
// @homepageURL  https://gitlab.com/fellomen/userscripts
// ==/UserScript==

(() => {
  "use strict";
  /**
   * Data extracted from the preview iframe for game profiles.
   */
  class GameProfileIFrameData {
    /**
     * @param {(String|undefined)} animatedBackground - URL for the animated profiel background, if available.
     * @param {(String|undefined)} staticBackground - URL for the static profiel background, may also be set together with animatedBackground.
     * @param {(String|undefined)} avatarFrame - URL for the (animated) avatar frame, if available.
     * @param {(String|undefined)} avatar - URL for the (animated) avatar, if available.
     */
    constructor(animatedBackground, staticBackground, avatarFrame, avatar) {
      this.animatedBg = animatedBackground;
      this.staticBg = staticBackground;
      this.avatarFrame = avatarFrame;
      this.avatar = avatar;
    }

    /**
     * Whether the script was able to parse any data.
     * @returns {!boolean} true, if any assets were parsed; false otherwise.
     */
    get hasAnyValues() {
      return (
        this.animatedBg !== undefined &&
        this.staticBg !== undefined &&
        this.avatarFrame !== undefined &&
        this.avatar !== undefined
      );
    }
  }

  if (window.top !== window.self) {
    // we are running in an iframe, which means we are viewing a game profiel preview.
    // in here we can parse the needed data and smuggle it over to our main-page script.
    // or that would be the plan.
    const appId = document.location.href.match(/appid=(.+)[&$]/)[1];
    const parsedData = new GameProfileIFrameData();
    Congalala.select(".profile_animated_background > video", video => {
      // TODO: Configurable: webm/mp4
      parsedData.animatedBg = video.querySelector("source[type='video/webm']").src;
      parsedData.staticBg = video.poster;
    });
    Congalala.select(".profile_avatar_frame > img", img => (parsedData.avatarFrame = img.src));
    Congalala.select(".profile_avatar_frame + img", img => (parsedData.avatar = img.src));

    if (parsedData.hasAnyValues) {
      console.info("Saving parsed values for app id: " + appId);
      GM_setValue(appId, JSON.stringify(parsedData));
    } else {
      console.warn("Ran in iframe, but could not parse any game profile data.");
    }
    return;
  }

  GM_setValue(getAppId(), undefined); // Ensure iframe data is reset

  const Selectors = [
    ".ModalOverlayContent.active .avatarFrame img", // Avatar Frames (must be ahead of avatars)
    ".ModalOverlayContent.active .avatarHolder img", // (Animated) Avatars
    ".ModalOverlayContent.active div[style^='background-image']", // Static Profile Backgrounds
    ".ModalOverlayContent.active video", // Animated Profile Backgrounds
  ];

  const Placeholders = {
    AppName: "{appname}",
    AppId: "{appid}",
    MediaName: "{medianame}",
    MediaType: "{mediatype}",
    Extension: "{extension}",
  };

  /**
   * Set dlTimestamp globally here because currently I only
   * see a way to manually download them one by one.
   * @type {!string}
   */
  const dlTimestamp = Date.now();

  new Congalala(
    () => {
      for (const selector of Selectors) {
        const match = document.querySelector(selector);
        if (match) {
          const src = extractSrc(match);
          const parameters = buildBasicTemplateParameters(src);
          addMediaInformation(parameters);
          return [{ src: src, parameters: parameters }];
        }
      }
      // if nothing else matched we might be on a Game Profile
      const gameProfileFiles = getGameProfileAssets();
      return gameProfileFiles || [];
    },
    "Steam Point Shop/{appname}/{?mediatype }{medianame}.{ext}",
    Congalala.getPlaceholderValues(Placeholders),
  ).init(() => {
    console.log("Script initialization finished.");
  });

  /**
   * Extract URL to the media, depending on which element we get.
   * @param {!HTMLElement} element the element to parse from
   * @returns {(string|undefined)} the URL or undefined if it could not be extracted
   */
  function extractSrc(element) {
    if (element instanceof HTMLImageElement) {
      return element.src;
    } else if (element instanceof HTMLVideoElement) {
      /** @todo Make vide type configurable: webm or mp4 */
      return element.querySelector("source[type='video/webm']").src;
    } else if (element instanceof HTMLDivElement) {
      return element.style.backgroundImage.match('url\\("(.*)"')[1];
    }
    return undefined;
  }

  /**
   * Attempt to collect all files for a Game Profile.
   * A "companion" script runs inside the iframe of the game preview
   * and parses the page for the actual URLs to various stuff.
   * @returns (FileInfo[]|undefined) an array containing FileInfo objects for asset in the profile; or undefined if no data is available
   */
  function getGameProfileAssets() {
    const base = document.querySelector(".ModalOverlayContent.active");
    const anchor = base.querySelector("div[title]").parentElement;
    const appName = base.querySelector("a").innerText;
    const profileName = anchor.childNodes[0].textContent;
    /** @type {?GameProfileIFrameData} */
    const dataFromIframe = parseCompanionData();
    if (dataFromIframe === undefined) {
      return undefined;
    }
    const files = [];
    addFileInfo(files, dataFromIframe.animatedBg, appName, profileName + " Animated Background");
    addFileInfo(files, dataFromIframe.staticBg, appName, profileName + " Static Background");
    addFileInfo(files, dataFromIframe.avatarFrame, appName, profileName + " Avatar Frame");
    addFileInfo(files, dataFromIframe.avatar, appName, profileName + " Avatar");
    return files;

    /**
     * Convenience method to add `dataFromIframe` to `files`.
     * @param {!FileInfo[]} files - the array to add elements to
     * @param {(string|undefined)} src - the URL where to download the asset from
     */
    function addFileInfo(files, src, appName, mediaName) {
      if (src) {
        files.push({
          src: src,
          parameters: buildBasicTemplateParameters(src, appName, mediaName),
        });
      }
    }

    /**
     * Parses data from userscript storage that was (hopefully) provided by the
     * iframe companion.
     * @returns {}(GameProfileIFrameData|undefined)}
     */
    function parseCompanionData() {
      try {
        const json = GM_getValue(getAppId());
        const raw = JSON.parse(json);
        return Object.assign(GameProfileIFrameData.prototype, raw);
      } catch (ex) {
        // TODO: Debug logging.
        return undefined;
      }
    }
  }

  /**
   * Assembles all template parameters for the currently active preview.
   * @param {!string} src - URL to the file to download
   * @param {?string} appName - value for {@link Placeholders.AppName}
   * @param {?string} mediaName - value for {@link Placeholders.MediaName}
   * @param {?string} mediaType - value for {@link Placeholders.MediaType}
   * @returns {!TemplateParameters} parameters for the currently previewing item
   */
  function buildBasicTemplateParameters(src, appName, mediaName, mediaType) {
    const parameters = Congalala.createDefaultTemplateParameters(
      Congalala.urltoa(src),
      dlTimestamp,
    );
    parameters.addParameter(Placeholders.AppId, getAppId());
    parameters.addParameter(Placeholders.AppName, appName);
    parameters.addParameter(Placeholders.MediaName, mediaName);
    parameters.addParameter(Placeholders.MediaType, mediaType);
    return parameters;
  }

  function getAppId() {
    const appId = document.location.href.match(/\/(\d+)\/?/)[1];
    return appId;
  }

  /**
   * Fetches meta data from currently previewed point shop item.
   * @param {!TemplateParameters} parameters the TemplateParameters instance add the values to
   * @returns {!TemplateParameters} parameters with filled in:
   *   - {@link Placeholders.AppName}
   *   - {@link Placeholders.MediaName}
   *   - {@link Placeholders.MediaType}
   */
  function addMediaInformation(parameters) {
    // gotta love shitty ass minified/obfuscated CSS classes
    // anchor ourselves to the copy link element and let the
    // code monkey swing through the element jungle.
    const copyToClipboard = document.querySelector(".ModalOverlayContent.active div[title]");
    const mediaName = copyToClipboard.previousSibling.textContent;
    const extraInfo = copyToClipboard.parentElement.nextSibling;
    const mediaType = extraInfo.innerText;
    const appName = extraInfo.nextSibling.querySelector("img + div").innerText;
    parameters.addParameter(Placeholders.AppName, appName);
    parameters.addParameter(Placeholders.MediaType, mediaType);
    parameters.addParameter(Placeholders.MediaName, mediaName);
    return parameters;
  }
})();
